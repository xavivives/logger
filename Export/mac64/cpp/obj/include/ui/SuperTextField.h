#ifndef INCLUDED_ui_SuperTextField
#define INCLUDED_ui_SuperTextField

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flash/display/Sprite.h>
HX_DECLARE_CLASS2(flash,display,DisplayObject)
HX_DECLARE_CLASS2(flash,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(flash,display,IBitmapDrawable)
HX_DECLARE_CLASS2(flash,display,InteractiveObject)
HX_DECLARE_CLASS2(flash,display,Sprite)
HX_DECLARE_CLASS2(flash,events,EventDispatcher)
HX_DECLARE_CLASS2(flash,events,IEventDispatcher)
HX_DECLARE_CLASS2(flash,text,TextField)
HX_DECLARE_CLASS1(ui,SuperTextField)
namespace ui{


class HXCPP_CLASS_ATTRIBUTES  SuperTextField_obj : public ::flash::display::Sprite_obj{
	public:
		typedef ::flash::display::Sprite_obj super;
		typedef SuperTextField_obj OBJ_;
		SuperTextField_obj();
		Void __construct(::flash::text::TextField _textField);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< SuperTextField_obj > __new(::flash::text::TextField _textField);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~SuperTextField_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("SuperTextField"); }

		::flash::text::TextField textField;
		virtual Void selectAll( int start,int end);
		Dynamic selectAll_dyn();

};

} // end namespace ui

#endif /* INCLUDED_ui_SuperTextField */ 
