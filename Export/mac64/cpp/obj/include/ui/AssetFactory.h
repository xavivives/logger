#ifndef INCLUDED_ui_AssetFactory
#define INCLUDED_ui_AssetFactory

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(flash,display,DisplayObject)
HX_DECLARE_CLASS2(flash,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(flash,display,IBitmapDrawable)
HX_DECLARE_CLASS2(flash,display,InteractiveObject)
HX_DECLARE_CLASS2(flash,display,Sprite)
HX_DECLARE_CLASS2(flash,events,EventDispatcher)
HX_DECLARE_CLASS2(flash,events,IEventDispatcher)
HX_DECLARE_CLASS1(ui,AssetFactory)
namespace ui{


class HXCPP_CLASS_ATTRIBUTES  AssetFactory_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef AssetFactory_obj OBJ_;
		AssetFactory_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=false)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< AssetFactory_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~AssetFactory_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("AssetFactory"); }

		static Float scale;
		static Void init( );
		static Dynamic init_dyn();

		static ::flash::display::Sprite getAsset( ::String path);
		static Dynamic getAsset_dyn();

		static ::flash::display::Sprite getSvg( ::String path,Dynamic scale);
		static Dynamic getSvg_dyn();

		static ::flash::display::Sprite getBitmap( ::String path);
		static Dynamic getBitmap_dyn();

		static Float getScale( );
		static Dynamic getScale_dyn();

};

} // end namespace ui

#endif /* INCLUDED_ui_AssetFactory */ 
