#ifndef INCLUDED_ui_HelveticaNeueLight
#define INCLUDED_ui_HelveticaNeueLight

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flash/text/Font.h>
HX_DECLARE_CLASS2(flash,text,Font)
HX_DECLARE_CLASS2(flash,text,FontStyle)
HX_DECLARE_CLASS2(flash,text,FontType)
HX_DECLARE_CLASS1(ui,HelveticaNeueLight)
namespace ui{


class HXCPP_CLASS_ATTRIBUTES  HelveticaNeueLight_obj : public ::flash::text::Font_obj{
	public:
		typedef ::flash::text::Font_obj super;
		typedef HelveticaNeueLight_obj OBJ_;
		HelveticaNeueLight_obj();
		Void __construct(::String __o_filename,::flash::text::FontStyle style,::flash::text::FontType type);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< HelveticaNeueLight_obj > __new(::String __o_filename,::flash::text::FontStyle style,::flash::text::FontType type);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~HelveticaNeueLight_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("HelveticaNeueLight"); }

		static ::String resourceName;
};

} // end namespace ui

#endif /* INCLUDED_ui_HelveticaNeueLight */ 
