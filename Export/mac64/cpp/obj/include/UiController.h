#ifndef INCLUDED_UiController
#define INCLUDED_UiController

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flash/display/Sprite.h>
HX_DECLARE_CLASS0(BaseButton)
HX_DECLARE_CLASS0(TagController)
HX_DECLARE_CLASS0(UiController)
HX_DECLARE_CLASS2(flash,display,Bitmap)
HX_DECLARE_CLASS2(flash,display,DisplayObject)
HX_DECLARE_CLASS2(flash,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(flash,display,IBitmapDrawable)
HX_DECLARE_CLASS2(flash,display,InteractiveObject)
HX_DECLARE_CLASS2(flash,display,Sprite)
HX_DECLARE_CLASS2(flash,events,Event)
HX_DECLARE_CLASS2(flash,events,EventDispatcher)
HX_DECLARE_CLASS2(flash,events,IEventDispatcher)
HX_DECLARE_CLASS2(flash,events,MouseEvent)


class HXCPP_CLASS_ATTRIBUTES  UiController_obj : public ::flash::display::Sprite_obj{
	public:
		typedef ::flash::display::Sprite_obj super;
		typedef UiController_obj OBJ_;
		UiController_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< UiController_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~UiController_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("UiController"); }

		::TagController tagController;
		::flash::display::Bitmap bitmap;
		Float prevMouseY;
		Float velY;
		::BaseButton addButton;
		Float scale;
		virtual Void init( );
		Dynamic init_dyn();

		virtual Void onMouseDown( ::flash::events::MouseEvent event);
		Dynamic onMouseDown_dyn();

		virtual Void onMouseMove( ::flash::events::MouseEvent event);
		Dynamic onMouseMove_dyn();

		virtual Void onMouseUp( ::flash::events::MouseEvent event);
		Dynamic onMouseUp_dyn();

		virtual Void onEnterFrame( ::flash::events::Event event);
		Dynamic onEnterFrame_dyn();

		virtual Void initAddButton( );
		Dynamic initAddButton_dyn();

		virtual Void onAddButton( );
		Dynamic onAddButton_dyn();

};


#endif /* INCLUDED_UiController */ 
