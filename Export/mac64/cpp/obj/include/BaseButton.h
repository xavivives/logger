#ifndef INCLUDED_BaseButton
#define INCLUDED_BaseButton

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flash/display/Sprite.h>
HX_DECLARE_CLASS0(BaseButton)
HX_DECLARE_CLASS2(flash,display,DisplayObject)
HX_DECLARE_CLASS2(flash,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(flash,display,IBitmapDrawable)
HX_DECLARE_CLASS2(flash,display,InteractiveObject)
HX_DECLARE_CLASS2(flash,display,Sprite)
HX_DECLARE_CLASS2(flash,events,EventDispatcher)
HX_DECLARE_CLASS2(flash,events,IEventDispatcher)


class HXCPP_CLASS_ATTRIBUTES  BaseButton_obj : public ::flash::display::Sprite_obj{
	public:
		typedef ::flash::display::Sprite_obj super;
		typedef BaseButton_obj OBJ_;
		BaseButton_obj();
		Void __construct(::flash::display::Sprite _asset,Dynamic onClickCallback);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< BaseButton_obj > __new(::flash::display::Sprite _asset,Dynamic onClickCallback);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~BaseButton_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("BaseButton"); }

		::flash::display::Sprite asset;
		::flash::display::Sprite hitArea;
		bool selected;
		Dynamic onClickCallback;
		Dynamic &onClickCallback_dyn() { return onClickCallback;}
		virtual Void enable( );
		Dynamic enable_dyn();

		virtual Void disable( );
		Dynamic disable_dyn();

		virtual Void initHitArea( );
		Dynamic initHitArea_dyn();

		virtual Void setEvents( );
		Dynamic setEvents_dyn();

		virtual Void onOver( Dynamic _);
		Dynamic onOver_dyn();

		virtual Void onOut( Dynamic _);
		Dynamic onOut_dyn();

		virtual Void onClick( Dynamic _);
		Dynamic onClick_dyn();

};


#endif /* INCLUDED_BaseButton */ 
