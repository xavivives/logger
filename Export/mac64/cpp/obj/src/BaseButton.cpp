#include <hxcpp.h>

#ifndef INCLUDED_BaseButton
#include <BaseButton.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObject
#include <flash/display/DisplayObject.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObjectContainer
#include <flash/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_flash_display_Graphics
#include <flash/display/Graphics.h>
#endif
#ifndef INCLUDED_flash_display_IBitmapDrawable
#include <flash/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_flash_display_InteractiveObject
#include <flash/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_flash_display_Sprite
#include <flash/display/Sprite.h>
#endif
#ifndef INCLUDED_flash_events_Event
#include <flash/events/Event.h>
#endif
#ifndef INCLUDED_flash_events_EventDispatcher
#include <flash/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_IEventDispatcher
#include <flash/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_MouseEvent
#include <flash/events/MouseEvent.h>
#endif

Void BaseButton_obj::__construct(::flash::display::Sprite _asset,Dynamic onClickCallback)
{
HX_STACK_FRAME("BaseButton","new",0x93e78375,"BaseButton.new","BaseButton.hx",21,0x9832619b)
HX_STACK_THIS(this)
HX_STACK_ARG(_asset,"_asset")
HX_STACK_ARG(onClickCallback,"onClickCallback")
{
	HX_STACK_LINE(22)
	super::__construct();
	HX_STACK_LINE(23)
	this->asset = _asset;
	HX_STACK_LINE(24)
	this->addChild(this->asset);
	HX_STACK_LINE(26)
	this->onClickCallback = onClickCallback;
	HX_STACK_LINE(27)
	this->initHitArea();
	HX_STACK_LINE(28)
	this->setEvents();
}
;
	return null();
}

//BaseButton_obj::~BaseButton_obj() { }

Dynamic BaseButton_obj::__CreateEmpty() { return  new BaseButton_obj; }
hx::ObjectPtr< BaseButton_obj > BaseButton_obj::__new(::flash::display::Sprite _asset,Dynamic onClickCallback)
{  hx::ObjectPtr< BaseButton_obj > result = new BaseButton_obj();
	result->__construct(_asset,onClickCallback);
	return result;}

Dynamic BaseButton_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< BaseButton_obj > result = new BaseButton_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

Void BaseButton_obj::enable( ){
{
		HX_STACK_FRAME("BaseButton","enable",0x01e7568e,"BaseButton.enable","BaseButton.hx",32,0x9832619b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(33)
		this->setEvents();
		HX_STACK_LINE(34)
		this->asset->set_alpha((int)1);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(BaseButton_obj,enable,(void))

Void BaseButton_obj::disable( ){
{
		HX_STACK_FRAME("BaseButton","disable",0xdbabcb7d,"BaseButton.disable","BaseButton.hx",37,0x9832619b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(38)
		this->hitArea->removeEventListener(::flash::events::MouseEvent_obj::CLICK,this->onClick_dyn(),null());
		HX_STACK_LINE(39)
		this->hitArea->removeEventListener(::flash::events::MouseEvent_obj::MOUSE_OVER,this->onOver_dyn(),null());
		HX_STACK_LINE(40)
		this->hitArea->removeEventListener(::flash::events::MouseEvent_obj::MOUSE_OUT,this->onOut_dyn(),null());
		HX_STACK_LINE(41)
		this->asset->set_alpha(0.5);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(BaseButton_obj,disable,(void))

Void BaseButton_obj::initHitArea( ){
{
		HX_STACK_FRAME("BaseButton","initHitArea",0x8730e265,"BaseButton.initHitArea","BaseButton.hx",45,0x9832619b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(46)
		::flash::display::Sprite _g = ::flash::display::Sprite_obj::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(46)
		this->hitArea = _g;
		HX_STACK_LINE(47)
		this->hitArea->get_graphics()->beginFill((int)16711680,0.5);
		HX_STACK_LINE(48)
		Float _g1 = this->asset->get_width();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(48)
		Float _g2 = this->asset->get_height();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(48)
		this->hitArea->get_graphics()->drawRect((int)0,(int)0,_g1,_g2);
		HX_STACK_LINE(49)
		this->addChild(this->hitArea);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(BaseButton_obj,initHitArea,(void))

Void BaseButton_obj::setEvents( ){
{
		HX_STACK_FRAME("BaseButton","setEvents",0xe5307530,"BaseButton.setEvents","BaseButton.hx",53,0x9832619b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(54)
		this->hitArea->addEventListener(::flash::events::MouseEvent_obj::CLICK,this->onClick_dyn(),null(),null(),null());
		HX_STACK_LINE(55)
		this->hitArea->addEventListener(::flash::events::MouseEvent_obj::MOUSE_OVER,this->onOver_dyn(),null(),null(),null());
		HX_STACK_LINE(56)
		this->hitArea->addEventListener(::flash::events::MouseEvent_obj::MOUSE_OUT,this->onOut_dyn(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(BaseButton_obj,setEvents,(void))

Void BaseButton_obj::onOver( Dynamic _){
{
		HX_STACK_FRAME("BaseButton","onOver",0xf5a2421e,"BaseButton.onOver","BaseButton.hx",60,0x9832619b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(_,"_")
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(BaseButton_obj,onOver,(void))

Void BaseButton_obj::onOut( Dynamic _){
{
		HX_STACK_FRAME("BaseButton","onOut",0x98a29704,"BaseButton.onOut","BaseButton.hx",64,0x9832619b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(_,"_")
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(BaseButton_obj,onOut,(void))

Void BaseButton_obj::onClick( Dynamic _){
{
		HX_STACK_FRAME("BaseButton","onClick",0x08ef7c3e,"BaseButton.onClick","BaseButton.hx",71,0x9832619b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(_,"_")
		HX_STACK_LINE(71)
		this->onClickCallback();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(BaseButton_obj,onClick,(void))


BaseButton_obj::BaseButton_obj()
{
}

void BaseButton_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(BaseButton);
	HX_MARK_MEMBER_NAME(asset,"asset");
	HX_MARK_MEMBER_NAME(hitArea,"hitArea");
	HX_MARK_MEMBER_NAME(selected,"selected");
	HX_MARK_MEMBER_NAME(onClickCallback,"onClickCallback");
	::flash::display::DisplayObjectContainer_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void BaseButton_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(asset,"asset");
	HX_VISIT_MEMBER_NAME(hitArea,"hitArea");
	HX_VISIT_MEMBER_NAME(selected,"selected");
	HX_VISIT_MEMBER_NAME(onClickCallback,"onClickCallback");
	::flash::display::DisplayObjectContainer_obj::__Visit(HX_VISIT_ARG);
}

Dynamic BaseButton_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"asset") ) { return asset; }
		if (HX_FIELD_EQ(inName,"onOut") ) { return onOut_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"enable") ) { return enable_dyn(); }
		if (HX_FIELD_EQ(inName,"onOver") ) { return onOver_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitArea") ) { return hitArea; }
		if (HX_FIELD_EQ(inName,"disable") ) { return disable_dyn(); }
		if (HX_FIELD_EQ(inName,"onClick") ) { return onClick_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"selected") ) { return selected; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"setEvents") ) { return setEvents_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"initHitArea") ) { return initHitArea_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"onClickCallback") ) { return onClickCallback; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic BaseButton_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"asset") ) { asset=inValue.Cast< ::flash::display::Sprite >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitArea") ) { hitArea=inValue.Cast< ::flash::display::Sprite >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"selected") ) { selected=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"onClickCallback") ) { onClickCallback=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void BaseButton_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("asset"));
	outFields->push(HX_CSTRING("hitArea"));
	outFields->push(HX_CSTRING("selected"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flash::display::Sprite*/ ,(int)offsetof(BaseButton_obj,asset),HX_CSTRING("asset")},
	{hx::fsObject /*::flash::display::Sprite*/ ,(int)offsetof(BaseButton_obj,hitArea),HX_CSTRING("hitArea")},
	{hx::fsBool,(int)offsetof(BaseButton_obj,selected),HX_CSTRING("selected")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(BaseButton_obj,onClickCallback),HX_CSTRING("onClickCallback")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("asset"),
	HX_CSTRING("hitArea"),
	HX_CSTRING("selected"),
	HX_CSTRING("onClickCallback"),
	HX_CSTRING("enable"),
	HX_CSTRING("disable"),
	HX_CSTRING("initHitArea"),
	HX_CSTRING("setEvents"),
	HX_CSTRING("onOver"),
	HX_CSTRING("onOut"),
	HX_CSTRING("onClick"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(BaseButton_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(BaseButton_obj::__mClass,"__mClass");
};

#endif

Class BaseButton_obj::__mClass;

void BaseButton_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("BaseButton"), hx::TCanCast< BaseButton_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void BaseButton_obj::__boot()
{
}

