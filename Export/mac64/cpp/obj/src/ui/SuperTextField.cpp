#include <hxcpp.h>

#ifndef INCLUDED_flash_display_DisplayObject
#include <flash/display/DisplayObject.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObjectContainer
#include <flash/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_flash_display_Graphics
#include <flash/display/Graphics.h>
#endif
#ifndef INCLUDED_flash_display_IBitmapDrawable
#include <flash/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_flash_display_InteractiveObject
#include <flash/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_flash_display_Sprite
#include <flash/display/Sprite.h>
#endif
#ifndef INCLUDED_flash_events_EventDispatcher
#include <flash/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_IEventDispatcher
#include <flash/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_flash_text_TextField
#include <flash/text/TextField.h>
#endif
#ifndef INCLUDED_flash_text_TextLineMetrics
#include <flash/text/TextLineMetrics.h>
#endif
#ifndef INCLUDED_ui_SuperTextField
#include <ui/SuperTextField.h>
#endif
namespace ui{

Void SuperTextField_obj::__construct(::flash::text::TextField _textField)
{
HX_STACK_FRAME("ui.SuperTextField","new",0xd30793be,"ui.SuperTextField.new","ui/SuperTextField.hx",18,0x71789c51)
HX_STACK_THIS(this)
HX_STACK_ARG(_textField,"_textField")
{
	HX_STACK_LINE(19)
	super::__construct();
	HX_STACK_LINE(20)
	this->textField = _textField;
	HX_STACK_LINE(21)
	this->addChild(this->textField);
}
;
	return null();
}

//SuperTextField_obj::~SuperTextField_obj() { }

Dynamic SuperTextField_obj::__CreateEmpty() { return  new SuperTextField_obj; }
hx::ObjectPtr< SuperTextField_obj > SuperTextField_obj::__new(::flash::text::TextField _textField)
{  hx::ObjectPtr< SuperTextField_obj > result = new SuperTextField_obj();
	result->__construct(_textField);
	return result;}

Dynamic SuperTextField_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< SuperTextField_obj > result = new SuperTextField_obj();
	result->__construct(inArgs[0]);
	return result;}

Void SuperTextField_obj::selectAll( int start,int end){
{
		HX_STACK_FRAME("ui.SuperTextField","selectAll",0x07463963,"ui.SuperTextField.selectAll","ui/SuperTextField.hx",26,0x71789c51)
		HX_STACK_THIS(this)
		HX_STACK_ARG(start,"start")
		HX_STACK_ARG(end,"end")
		HX_STACK_LINE(28)
		::flash::text::TextLineMetrics lineMerics = this->textField->getLineMetrics((int)0);		HX_STACK_VAR(lineMerics,"lineMerics");
		HX_STACK_LINE(29)
		Float w = lineMerics->width;		HX_STACK_VAR(w,"w");
		HX_STACK_LINE(30)
		Float h = lineMerics->height;		HX_STACK_VAR(h,"h");
		HX_STACK_LINE(31)
		Float x = (lineMerics->x + (int)2);		HX_STACK_VAR(x,"x");
		HX_STACK_LINE(32)
		int y = (int)2;		HX_STACK_VAR(y,"y");
		HX_STACK_LINE(34)
		this->get_graphics()->beginFill((int)16777215,0.5);
		HX_STACK_LINE(35)
		this->get_graphics()->moveTo((int)0,(int)0);
		HX_STACK_LINE(36)
		this->get_graphics()->drawRect(x,y,w,h);
		HX_STACK_LINE(39)
		this->get_graphics()->lineTo((int)0,(int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(SuperTextField_obj,selectAll,(void))


SuperTextField_obj::SuperTextField_obj()
{
}

void SuperTextField_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(SuperTextField);
	HX_MARK_MEMBER_NAME(textField,"textField");
	::flash::display::DisplayObjectContainer_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void SuperTextField_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(textField,"textField");
	::flash::display::DisplayObjectContainer_obj::__Visit(HX_VISIT_ARG);
}

Dynamic SuperTextField_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"textField") ) { return textField; }
		if (HX_FIELD_EQ(inName,"selectAll") ) { return selectAll_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic SuperTextField_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"textField") ) { textField=inValue.Cast< ::flash::text::TextField >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void SuperTextField_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("textField"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flash::text::TextField*/ ,(int)offsetof(SuperTextField_obj,textField),HX_CSTRING("textField")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("textField"),
	HX_CSTRING("selectAll"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(SuperTextField_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(SuperTextField_obj::__mClass,"__mClass");
};

#endif

Class SuperTextField_obj::__mClass;

void SuperTextField_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("ui.SuperTextField"), hx::TCanCast< SuperTextField_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void SuperTextField_obj::__boot()
{
}

} // end namespace ui
