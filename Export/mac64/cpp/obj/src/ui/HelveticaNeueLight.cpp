#include <hxcpp.h>

#ifndef INCLUDED_flash_text_Font
#include <flash/text/Font.h>
#endif
#ifndef INCLUDED_flash_text_FontStyle
#include <flash/text/FontStyle.h>
#endif
#ifndef INCLUDED_flash_text_FontType
#include <flash/text/FontType.h>
#endif
#ifndef INCLUDED_ui_HelveticaNeueLight
#include <ui/HelveticaNeueLight.h>
#endif
namespace ui{

Void HelveticaNeueLight_obj::__construct(::String __o_filename,::flash::text::FontStyle style,::flash::text::FontType type)
{
HX_STACK_FRAME("ui.HelveticaNeueLight","new",0x1233bfaa,"ui.HelveticaNeueLight.new","ui/UiTag.hx",14,0x3ea2e093)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_filename,"filename")
HX_STACK_ARG(style,"style")
HX_STACK_ARG(type,"type")
::String filename = __o_filename.Default(HX_CSTRING(""));
{
	HX_STACK_LINE(14)
	super::__construct(filename,style,type);
}
;
	return null();
}

//HelveticaNeueLight_obj::~HelveticaNeueLight_obj() { }

Dynamic HelveticaNeueLight_obj::__CreateEmpty() { return  new HelveticaNeueLight_obj; }
hx::ObjectPtr< HelveticaNeueLight_obj > HelveticaNeueLight_obj::__new(::String __o_filename,::flash::text::FontStyle style,::flash::text::FontType type)
{  hx::ObjectPtr< HelveticaNeueLight_obj > result = new HelveticaNeueLight_obj();
	result->__construct(__o_filename,style,type);
	return result;}

Dynamic HelveticaNeueLight_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< HelveticaNeueLight_obj > result = new HelveticaNeueLight_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

::String HelveticaNeueLight_obj::resourceName;


HelveticaNeueLight_obj::HelveticaNeueLight_obj()
{
}

Dynamic HelveticaNeueLight_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 12:
		if (HX_FIELD_EQ(inName,"resourceName") ) { return resourceName; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic HelveticaNeueLight_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 12:
		if (HX_FIELD_EQ(inName,"resourceName") ) { resourceName=inValue.Cast< ::String >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void HelveticaNeueLight_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("resourceName"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(HelveticaNeueLight_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(HelveticaNeueLight_obj::resourceName,"resourceName");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(HelveticaNeueLight_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(HelveticaNeueLight_obj::resourceName,"resourceName");
};

#endif

Class HelveticaNeueLight_obj::__mClass;

void HelveticaNeueLight_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("ui.HelveticaNeueLight"), hx::TCanCast< HelveticaNeueLight_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void HelveticaNeueLight_obj::__boot()
{
	resourceName= HX_CSTRING("NME_font_ui_HelveticaNeueLight");
}

} // end namespace ui
