#include <hxcpp.h>

#ifndef INCLUDED_BaseButton
#include <BaseButton.h>
#endif
#ifndef INCLUDED_Date
#include <Date.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_TagController
#include <TagController.h>
#endif
#ifndef INCLUDED_UiController
#include <UiController.h>
#endif
#ifndef INCLUDED_flash_display_Bitmap
#include <flash/display/Bitmap.h>
#endif
#ifndef INCLUDED_flash_display_BitmapData
#include <flash/display/BitmapData.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObject
#include <flash/display/DisplayObject.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObjectContainer
#include <flash/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_flash_display_IBitmapDrawable
#include <flash/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_flash_display_InteractiveObject
#include <flash/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_flash_display_PixelSnapping
#include <flash/display/PixelSnapping.h>
#endif
#ifndef INCLUDED_flash_display_Sprite
#include <flash/display/Sprite.h>
#endif
#ifndef INCLUDED_flash_display_Stage
#include <flash/display/Stage.h>
#endif
#ifndef INCLUDED_flash_events_Event
#include <flash/events/Event.h>
#endif
#ifndef INCLUDED_flash_events_EventDispatcher
#include <flash/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_IEventDispatcher
#include <flash/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_MouseEvent
#include <flash/events/MouseEvent.h>
#endif
#ifndef INCLUDED_flash_system_Capabilities
#include <flash/system/Capabilities.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_openfl_Assets
#include <openfl/Assets.h>
#endif

Void UiController_obj::__construct()
{
HX_STACK_FRAME("UiController","new",0xfdb07922,"UiController.new","UiController.hx",26,0x7e44808e)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(26)
	super::__construct();
}
;
	return null();
}

//UiController_obj::~UiController_obj() { }

Dynamic UiController_obj::__CreateEmpty() { return  new UiController_obj; }
hx::ObjectPtr< UiController_obj > UiController_obj::__new()
{  hx::ObjectPtr< UiController_obj > result = new UiController_obj();
	result->__construct();
	return result;}

Dynamic UiController_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< UiController_obj > result = new UiController_obj();
	result->__construct();
	return result;}

Void UiController_obj::init( ){
{
		HX_STACK_FRAME("UiController","init",0xf9723c0e,"UiController.init","UiController.hx",30,0x7e44808e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(31)
		this->velY = (int)0;
		HX_STACK_LINE(33)
		Float _g = ::flash::system::Capabilities_obj::get_screenDPI();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(33)
		Float _g1 = (Float(_g) / Float((int)126));		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(33)
		this->scale = _g1;
		HX_STACK_LINE(34)
		this->set_scaleX(this->scale);
		HX_STACK_LINE(35)
		this->set_scaleY(this->scale);
		HX_STACK_LINE(37)
		::flash::display::BitmapData _g2 = ::openfl::Assets_obj::getBitmapData(HX_CSTRING("assets/background.jpg"),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(37)
		::flash::display::Bitmap _g3 = ::flash::display::Bitmap_obj::__new(_g2,null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(37)
		this->bitmap = _g3;
		HX_STACK_LINE(38)
		this->bitmap->set_smoothing(true);
		HX_STACK_LINE(40)
		this->addChild(this->bitmap);
		HX_STACK_LINE(44)
		int _g4 = this->get_stage()->get_stageWidth();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(44)
		Float _g5 = this->bitmap->get_width();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(44)
		Float _g6 = (_g4 - _g5);		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(44)
		Float _g7 = (Float(_g6) / Float((int)2));		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(44)
		this->bitmap->set_x(_g7);
		HX_STACK_LINE(45)
		int _g8 = this->get_stage()->get_stageHeight();		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(45)
		Float _g9 = this->bitmap->get_height();		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(45)
		Float _g10 = (_g8 - _g9);		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(45)
		Float _g11 = (Float(_g10) / Float((int)2));		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(45)
		this->bitmap->set_y(_g11);
		HX_STACK_LINE(47)
		int _g12 = this->get_stage()->get_stageWidth();		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(47)
		Float _g13 = (Float(_g12) / Float(this->scale));		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(47)
		this->bitmap->set_width(_g13);
		HX_STACK_LINE(48)
		Float _g14 = this->bitmap->get_scaleX();		HX_STACK_VAR(_g14,"_g14");
		HX_STACK_LINE(48)
		this->bitmap->set_scaleY(_g14);
		HX_STACK_LINE(51)
		this->get_stage()->addEventListener(::flash::events::MouseEvent_obj::MOUSE_DOWN,this->onMouseDown_dyn(),null(),null(),null());
		HX_STACK_LINE(52)
		this->get_stage()->addEventListener(::flash::events::Event_obj::ENTER_FRAME,this->onEnterFrame_dyn(),null(),null(),null());
		HX_STACK_LINE(55)
		this->initAddButton();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UiController_obj,init,(void))

Void UiController_obj::onMouseDown( ::flash::events::MouseEvent event){
{
		HX_STACK_FRAME("UiController","onMouseDown",0x123bceca,"UiController.onMouseDown","UiController.hx",57,0x7e44808e)
		HX_STACK_THIS(this)
		HX_STACK_ARG(event,"event")
		HX_STACK_LINE(59)
		this->get_stage()->addEventListener(::flash::events::MouseEvent_obj::MOUSE_MOVE,this->onMouseMove_dyn(),null(),null(),null());
		HX_STACK_LINE(60)
		this->get_stage()->addEventListener(::flash::events::MouseEvent_obj::MOUSE_UP,this->onMouseUp_dyn(),null(),null(),null());
		HX_STACK_LINE(61)
		Float _g = this->get_stage()->get_mouseY();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(61)
		this->prevMouseY = _g;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(UiController_obj,onMouseDown,(void))

Void UiController_obj::onMouseMove( ::flash::events::MouseEvent event){
{
		HX_STACK_FRAME("UiController","onMouseMove",0x182eb979,"UiController.onMouseMove","UiController.hx",65,0x7e44808e)
		HX_STACK_THIS(this)
		HX_STACK_ARG(event,"event")
		HX_STACK_LINE(66)
		Float _g = this->get_stage()->get_mouseY();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(66)
		Float _g1 = (_g - this->prevMouseY);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(66)
		this->velY = _g1;
		HX_STACK_LINE(67)
		Float _g2 = this->get_stage()->get_mouseY();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(67)
		this->prevMouseY = _g2;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(UiController_obj,onMouseMove,(void))

Void UiController_obj::onMouseUp( ::flash::events::MouseEvent event){
{
		HX_STACK_FRAME("UiController","onMouseUp",0x9bac52c3,"UiController.onMouseUp","UiController.hx",71,0x7e44808e)
		HX_STACK_THIS(this)
		HX_STACK_ARG(event,"event")
		HX_STACK_LINE(73)
		this->get_stage()->removeEventListener(::flash::events::MouseEvent_obj::MOUSE_MOVE,this->onMouseMove_dyn(),null());
		HX_STACK_LINE(74)
		this->get_stage()->removeEventListener(::flash::events::MouseEvent_obj::MOUSE_UP,this->onMouseUp_dyn(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(UiController_obj,onMouseUp,(void))

Void UiController_obj::onEnterFrame( ::flash::events::Event event){
{
		HX_STACK_FRAME("UiController","onEnterFrame",0xe8d0d4f2,"UiController.onEnterFrame","UiController.hx",78,0x7e44808e)
		HX_STACK_THIS(this)
		HX_STACK_ARG(event,"event")
		HX_STACK_LINE(80)
		{
			HX_STACK_LINE(80)
			::flash::display::Bitmap _g = this->bitmap;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(80)
			Float _g1 = _g->get_y();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(80)
			Float _g11 = (_g1 + (Float(this->velY) / Float(this->scale)));		HX_STACK_VAR(_g11,"_g11");
			HX_STACK_LINE(80)
			_g->set_y(_g11);
		}
		HX_STACK_LINE(81)
		hx::MultEq(this->velY,0.9);
		HX_STACK_LINE(83)
		Float _g2 = ::Math_obj::abs(this->velY);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(83)
		if (((_g2 < (int)1))){
			HX_STACK_LINE(84)
			this->velY = (int)0;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(UiController_obj,onEnterFrame,(void))

Void UiController_obj::initAddButton( ){
{
		HX_STACK_FRAME("UiController","initAddButton",0x74504905,"UiController.initAddButton","UiController.hx",88,0x7e44808e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(89)
		::BaseButton _g = ::BaseButton_obj::__new(HX_CSTRING("assets/addButton.png"),this->onAddButton_dyn());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(89)
		this->addButton = _g;
		HX_STACK_LINE(90)
		this->addChild(this->addButton);
		HX_STACK_LINE(91)
		this->addButton->set_x((int)100);
		HX_STACK_LINE(92)
		this->addButton->set_y((int)100);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UiController_obj,initAddButton,(void))

Void UiController_obj::onAddButton( ){
{
		HX_STACK_FRAME("UiController","onAddButton",0xb57db496,"UiController.onAddButton","UiController.hx",96,0x7e44808e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(97)
		::haxe::Log_obj::trace(HX_CSTRING("Add button here!"),hx::SourceInfo(HX_CSTRING("UiController.hx"),97,HX_CSTRING("UiController"),HX_CSTRING("onAddButton")));
		HX_STACK_LINE(98)
		Float _g = ::flash::system::Capabilities_obj::get_screenDPI();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(98)
		::haxe::Log_obj::trace(_g,hx::SourceInfo(HX_CSTRING("UiController.hx"),98,HX_CSTRING("UiController"),HX_CSTRING("onAddButton")));
		HX_STACK_LINE(99)
		Float _g1 = ::Date_obj::now()->getTime();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(99)
		Float _g2 = (_g1 * (int)1000);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(99)
		int _g3 = ::Std_obj::_int(_g2);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(99)
		this->tagController->createNewTag(HX_CSTRING("Run"),_g3);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UiController_obj,onAddButton,(void))


UiController_obj::UiController_obj()
{
}

void UiController_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(UiController);
	HX_MARK_MEMBER_NAME(tagController,"tagController");
	HX_MARK_MEMBER_NAME(bitmap,"bitmap");
	HX_MARK_MEMBER_NAME(prevMouseY,"prevMouseY");
	HX_MARK_MEMBER_NAME(velY,"velY");
	HX_MARK_MEMBER_NAME(addButton,"addButton");
	HX_MARK_MEMBER_NAME(scale,"scale");
	::flash::display::DisplayObjectContainer_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void UiController_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(tagController,"tagController");
	HX_VISIT_MEMBER_NAME(bitmap,"bitmap");
	HX_VISIT_MEMBER_NAME(prevMouseY,"prevMouseY");
	HX_VISIT_MEMBER_NAME(velY,"velY");
	HX_VISIT_MEMBER_NAME(addButton,"addButton");
	HX_VISIT_MEMBER_NAME(scale,"scale");
	::flash::display::DisplayObjectContainer_obj::__Visit(HX_VISIT_ARG);
}

Dynamic UiController_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"velY") ) { return velY; }
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { return scale; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"bitmap") ) { return bitmap; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"addButton") ) { return addButton; }
		if (HX_FIELD_EQ(inName,"onMouseUp") ) { return onMouseUp_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"prevMouseY") ) { return prevMouseY; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"onMouseDown") ) { return onMouseDown_dyn(); }
		if (HX_FIELD_EQ(inName,"onMouseMove") ) { return onMouseMove_dyn(); }
		if (HX_FIELD_EQ(inName,"onAddButton") ) { return onAddButton_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"onEnterFrame") ) { return onEnterFrame_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"tagController") ) { return tagController; }
		if (HX_FIELD_EQ(inName,"initAddButton") ) { return initAddButton_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic UiController_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"velY") ) { velY=inValue.Cast< Float >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { scale=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"bitmap") ) { bitmap=inValue.Cast< ::flash::display::Bitmap >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"addButton") ) { addButton=inValue.Cast< ::BaseButton >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"prevMouseY") ) { prevMouseY=inValue.Cast< Float >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"tagController") ) { tagController=inValue.Cast< ::TagController >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void UiController_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("tagController"));
	outFields->push(HX_CSTRING("bitmap"));
	outFields->push(HX_CSTRING("prevMouseY"));
	outFields->push(HX_CSTRING("velY"));
	outFields->push(HX_CSTRING("addButton"));
	outFields->push(HX_CSTRING("scale"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::TagController*/ ,(int)offsetof(UiController_obj,tagController),HX_CSTRING("tagController")},
	{hx::fsObject /*::flash::display::Bitmap*/ ,(int)offsetof(UiController_obj,bitmap),HX_CSTRING("bitmap")},
	{hx::fsFloat,(int)offsetof(UiController_obj,prevMouseY),HX_CSTRING("prevMouseY")},
	{hx::fsFloat,(int)offsetof(UiController_obj,velY),HX_CSTRING("velY")},
	{hx::fsObject /*::BaseButton*/ ,(int)offsetof(UiController_obj,addButton),HX_CSTRING("addButton")},
	{hx::fsFloat,(int)offsetof(UiController_obj,scale),HX_CSTRING("scale")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("tagController"),
	HX_CSTRING("bitmap"),
	HX_CSTRING("prevMouseY"),
	HX_CSTRING("velY"),
	HX_CSTRING("addButton"),
	HX_CSTRING("scale"),
	HX_CSTRING("init"),
	HX_CSTRING("onMouseDown"),
	HX_CSTRING("onMouseMove"),
	HX_CSTRING("onMouseUp"),
	HX_CSTRING("onEnterFrame"),
	HX_CSTRING("initAddButton"),
	HX_CSTRING("onAddButton"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(UiController_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(UiController_obj::__mClass,"__mClass");
};

#endif

Class UiController_obj::__mClass;

void UiController_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("UiController"), hx::TCanCast< UiController_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void UiController_obj::__boot()
{
}

