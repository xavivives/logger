package;


import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.MovieClip;
import flash.text.Font;
import flash.media.Sound;
import flash.net.URLRequest;
import flash.utils.ByteArray;
import haxe.Unserializer;
import openfl.Assets;

#if (flash || js)
import flash.display.Loader;
import flash.events.Event;
import flash.net.URLLoader;
#end

#if ios
import openfl.utils.SystemPath;
#end


@:access(flash.media.Sound)
class DefaultAssetLibrary extends AssetLibrary {
	
	
	public static var className (default, null) = new Map <String, Dynamic> ();
	public static var path (default, null) = new Map <String, String> ();
	public static var type (default, null) = new Map <String, AssetType> ();
	
	public function new () {
		
		super ();
		
		#if flash
		
		className.set ("assets/addButton.png", __ASSET__assets_addbutton_png);
		type.set ("assets/addButton.png", Reflect.field (AssetType, "image".toUpperCase ()));
		className.set ("assets/background.jpg", __ASSET__assets_background_jpg);
		type.set ("assets/background.jpg", Reflect.field (AssetType, "image".toUpperCase ()));
		className.set ("assets/background2.jpg", __ASSET__assets_background2_jpg);
		type.set ("assets/background2.jpg", Reflect.field (AssetType, "image".toUpperCase ()));
		className.set ("assets/Components.fxg", __ASSET__assets_components_fxg);
		type.set ("assets/Components.fxg", Reflect.field (AssetType, "text".toUpperCase ()));
		className.set ("assets/helveticaneue.eot", __ASSET__assets_helveticaneue_eot);
		type.set ("assets/helveticaneue.eot", Reflect.field (AssetType, "binary".toUpperCase ()));
		className.set ("assets/helveticaneue.svg", __ASSET__assets_helveticaneue_svg);
		type.set ("assets/helveticaneue.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		className.set ("assets/helveticaneue.ttf", __ASSET__assets_helveticaneue_ttf);
		type.set ("assets/helveticaneue.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
		className.set ("assets/helveticaneue.woff", __ASSET__assets_helveticaneue_woff);
		type.set ("assets/helveticaneue.woff", Reflect.field (AssetType, "binary".toUpperCase ()));
		className.set ("assets/HelveticaNeueLight.eot", __ASSET__assets_helveticaneuelight_eot);
		type.set ("assets/HelveticaNeueLight.eot", Reflect.field (AssetType, "binary".toUpperCase ()));
		className.set ("assets/HelveticaNeueLight.svg", __ASSET__assets_helveticaneuelight_svg);
		type.set ("assets/HelveticaNeueLight.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		className.set ("assets/HelveticaNeueLight.ttf", __ASSET__assets_helveticaneuelight_ttf);
		type.set ("assets/HelveticaNeueLight.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
		className.set ("assets/HelveticaNeueLight.woff", __ASSET__assets_helveticaneuelight_woff);
		type.set ("assets/HelveticaNeueLight.woff", Reflect.field (AssetType, "binary".toUpperCase ()));
		className.set ("assets/tagBody.png", __ASSET__assets_tagbody_png);
		type.set ("assets/tagBody.png", Reflect.field (AssetType, "image".toUpperCase ()));
		className.set ("assets/tagBody.svg", __ASSET__assets_tagbody_svg);
		type.set ("assets/tagBody.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		className.set ("assets/tagCue.png", __ASSET__assets_tagcue_png);
		type.set ("assets/tagCue.png", Reflect.field (AssetType, "image".toUpperCase ()));
		className.set ("assets/tagCue.svg", __ASSET__assets_tagcue_svg);
		type.set ("assets/tagCue.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		className.set ("assets/tagTip.png", __ASSET__assets_tagtip_png);
		type.set ("assets/tagTip.png", Reflect.field (AssetType, "image".toUpperCase ()));
		className.set ("assets/tagTip.svg", __ASSET__assets_tagtip_svg);
		type.set ("assets/tagTip.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		className.set ("assets/test.svg", __ASSET__assets_test_svg);
		type.set ("assets/test.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		
		
		#elseif html5
		
		addExternal("assets/addButton.png", "image", "assets/addButton.png");
		addExternal("assets/background.jpg", "image", "assets/background.jpg");
		addExternal("assets/background2.jpg", "image", "assets/background2.jpg");
		addExternal("assets/Components.fxg", "text", "assets/Components.fxg");
		addExternal("assets/helveticaneue.eot", "binary", "assets/helveticaneue.eot");
		addExternal("assets/helveticaneue.svg", "text", "assets/helveticaneue.svg");
		addEmbed("assets/helveticaneue.ttf", "font", __ASSET__assets_helveticaneue_ttf);
		addExternal("assets/helveticaneue.woff", "binary", "assets/helveticaneue.woff");
		addExternal("assets/HelveticaNeueLight.eot", "binary", "assets/HelveticaNeueLight.eot");
		addExternal("assets/HelveticaNeueLight.svg", "text", "assets/HelveticaNeueLight.svg");
		addEmbed("assets/HelveticaNeueLight.ttf", "font", __ASSET__assets_helveticaneuelight_ttf);
		addExternal("assets/HelveticaNeueLight.woff", "binary", "assets/HelveticaNeueLight.woff");
		addExternal("assets/tagBody.png", "image", "assets/tagBody.png");
		addExternal("assets/tagBody.svg", "text", "assets/tagBody.svg");
		addExternal("assets/tagCue.png", "image", "assets/tagCue.png");
		addExternal("assets/tagCue.svg", "text", "assets/tagCue.svg");
		addExternal("assets/tagTip.png", "image", "assets/tagTip.png");
		addExternal("assets/tagTip.svg", "text", "assets/tagTip.svg");
		addExternal("assets/test.svg", "text", "assets/test.svg");
		
		
		#else
		
		#if (windows || mac || linux)
		
		var loadManifest = false;
		
		className.set ("assets/addButton.png", __ASSET__assets_addbutton_png);
		type.set ("assets/addButton.png", Reflect.field (AssetType, "image".toUpperCase ()));
		
		className.set ("assets/background.jpg", __ASSET__assets_background_jpg);
		type.set ("assets/background.jpg", Reflect.field (AssetType, "image".toUpperCase ()));
		
		className.set ("assets/background2.jpg", __ASSET__assets_background2_jpg);
		type.set ("assets/background2.jpg", Reflect.field (AssetType, "image".toUpperCase ()));
		
		className.set ("assets/Components.fxg", __ASSET__assets_components_fxg);
		type.set ("assets/Components.fxg", Reflect.field (AssetType, "text".toUpperCase ()));
		
		className.set ("assets/helveticaneue.eot", __ASSET__assets_helveticaneue_eot);
		type.set ("assets/helveticaneue.eot", Reflect.field (AssetType, "binary".toUpperCase ()));
		
		className.set ("assets/helveticaneue.svg", __ASSET__assets_helveticaneue_svg);
		type.set ("assets/helveticaneue.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		
		className.set ("assets/helveticaneue.ttf", __ASSET__assets_helveticaneue_ttf);
		type.set ("assets/helveticaneue.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
		
		className.set ("assets/helveticaneue.woff", __ASSET__assets_helveticaneue_woff);
		type.set ("assets/helveticaneue.woff", Reflect.field (AssetType, "binary".toUpperCase ()));
		
		className.set ("assets/HelveticaNeueLight.eot", __ASSET__assets_helveticaneuelight_eot);
		type.set ("assets/HelveticaNeueLight.eot", Reflect.field (AssetType, "binary".toUpperCase ()));
		
		className.set ("assets/HelveticaNeueLight.svg", __ASSET__assets_helveticaneuelight_svg);
		type.set ("assets/HelveticaNeueLight.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		
		className.set ("assets/HelveticaNeueLight.ttf", __ASSET__assets_helveticaneuelight_ttf);
		type.set ("assets/HelveticaNeueLight.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
		
		className.set ("assets/HelveticaNeueLight.woff", __ASSET__assets_helveticaneuelight_woff);
		type.set ("assets/HelveticaNeueLight.woff", Reflect.field (AssetType, "binary".toUpperCase ()));
		
		className.set ("assets/tagBody.png", __ASSET__assets_tagbody_png);
		type.set ("assets/tagBody.png", Reflect.field (AssetType, "image".toUpperCase ()));
		
		className.set ("assets/tagBody.svg", __ASSET__assets_tagbody_svg);
		type.set ("assets/tagBody.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		
		className.set ("assets/tagCue.png", __ASSET__assets_tagcue_png);
		type.set ("assets/tagCue.png", Reflect.field (AssetType, "image".toUpperCase ()));
		
		className.set ("assets/tagCue.svg", __ASSET__assets_tagcue_svg);
		type.set ("assets/tagCue.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		
		className.set ("assets/tagTip.png", __ASSET__assets_tagtip_png);
		type.set ("assets/tagTip.png", Reflect.field (AssetType, "image".toUpperCase ()));
		
		className.set ("assets/tagTip.svg", __ASSET__assets_tagtip_svg);
		type.set ("assets/tagTip.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		
		className.set ("assets/test.svg", __ASSET__assets_test_svg);
		type.set ("assets/test.svg", Reflect.field (AssetType, "text".toUpperCase ()));
		
		
		#else
		
		var loadManifest = true;
		
		#end
		
		if (loadManifest) {
			try {
				
				#if blackberry
				var bytes = ByteArray.readFile ("app/native/manifest");
				#elseif tizen
				var bytes = ByteArray.readFile ("../res/manifest");
				#elseif emscripten
				var bytes = ByteArray.readFile ("assets/manifest");
				#else
				var bytes = ByteArray.readFile ("manifest");
				#end
				
				if (bytes != null) {
					
					bytes.position = 0;
					
					if (bytes.length > 0) {
						
						var data = bytes.readUTFBytes (bytes.length);
						
						if (data != null && data.length > 0) {
							
							var manifest:Array<AssetData> = Unserializer.run (data);
							
							for (asset in manifest) {
								
								if (!className.exists(asset.id)) {
									
									path.set (asset.id, asset.path);
									type.set (asset.id, asset.type);
									
								}
							}
						
						}
					
					}
				
				} else {
				
					trace ("Warning: Could not load asset manifest");
				
				}
			
			} catch (e:Dynamic) {
			
				trace ("Warning: Could not load asset manifest");
			
			}
		
		}
		
		#end
		
	}
	
	
	#if html5
	private function addEmbed(id:String, kind:String, value:Dynamic):Void {
		className.set(id, value);
		type.set(id, Reflect.field(AssetType, kind.toUpperCase()));
	}
	
	
	private function addExternal(id:String, kind:String, value:String):Void {
		path.set(id, value);
		type.set(id, Reflect.field(AssetType, kind.toUpperCase()));
	}
	#end
	
	
	public override function exists (id:String, type:AssetType):Bool {
		
		var assetType = DefaultAssetLibrary.type.get (id);
		
		#if pixi
		
		if (assetType == IMAGE) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
		#end
		
		if (assetType != null) {
			
			if (assetType == type || ((type == SOUND || type == MUSIC) && (assetType == MUSIC || assetType == SOUND))) {
				
				return true;
				
			}
			
			#if flash
			
			if ((assetType == BINARY || assetType == TEXT) && type == BINARY) {
				
				return true;
				
			} else if (path.exists (id)) {
				
				return true;
				
			}
			
			#else
			
			if (type == BINARY || type == null) {
				
				return true;
				
			}
			
			#end
			
		}
		
		return false;
		
	}
	
	
	public override function getBitmapData (id:String):BitmapData {
		
		#if pixi
		
		return BitmapData.fromImage (path.get (id));
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), BitmapData);
		
		#elseif openfl_html5
		
		return BitmapData.fromImage (ApplicationMain.images.get (path.get (id)));
		
		#elseif js
		
		return cast (ApplicationMain.loaders.get (path.get (id)).contentLoaderInfo.content, Bitmap).bitmapData;
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), BitmapData);
		else return BitmapData.load (path.get (id));
		
		#end
		
	}
	
	
	public override function getBytes (id:String):ByteArray {
		
		#if pixi
		
		return null;
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), ByteArray);
		
		#elseif openfl_html5
		
		return null;
		
		#elseif js
		
		var bytes:ByteArray = null;
		var data = ApplicationMain.urlLoaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			bytes = new ByteArray ();
			bytes.writeUTFBytes (data);
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}

		if (bytes != null) {
			
			bytes.position = 0;
			return bytes;
			
		} else {
			
			return null;
		}
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), ByteArray);
		else return ByteArray.readFile (path.get (id));
		
		#end
		
	}
	
	
	public override function getFont (id:String):Font {
		
		#if pixi
		
		return null;
		
		#elseif (flash || js)
		
		return cast (Type.createInstance (className.get (id), []), Font);
		
		#else
		
		if (className.exists(id)) {
			var fontClass = className.get(id);
			Font.registerFont(fontClass);
			return cast (Type.createInstance (fontClass, []), Font);
		} else return new Font (path.get (id));
		
		#end
		
	}
	
	
	public override function getMusic (id:String):Sound {
		
		#if pixi
		
		return null;
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif openfl_html5
		
		var sound = new Sound ();
		sound.__buffer = true;
		sound.load (new URLRequest (path.get (id)));
		return sound; 
		
		#elseif js
		
		return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		else return new Sound (new URLRequest (path.get (id)), null, true);
		
		#end
		
	}
	
	
	public override function getPath (id:String):String {
		
		#if ios
		
		return SystemPath.applicationDirectory + "/assets/" + path.get (id);
		
		#else
		
		return path.get (id);
		
		#end
		
	}
	
	
	public override function getSound (id:String):Sound {
		
		#if pixi
		
		return null;
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif js
		
		return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		else return new Sound (new URLRequest (path.get (id)), null, type.get (id) == MUSIC);
		
		#end
		
	}
	
	
	public override function getText (id:String):String {
		
		#if js
		
		var bytes:ByteArray = null;
		var data = ApplicationMain.urlLoaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			return cast data;
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}
		
		if (bytes != null) {
			
			bytes.position = 0;
			return bytes.readUTFBytes (bytes.length);
			
		} else {
			
			return null;
		}
		
		#else
		
		var bytes = getBytes (id);
		
		if (bytes == null) {
			
			return null;
			
		} else {
			
			return bytes.readUTFBytes (bytes.length);
			
		}
		
		#end
		
	}
	
	
	public override function isLocal (id:String, type:AssetType):Bool {
		
		#if flash
		
		if (type != AssetType.MUSIC && type != AssetType.SOUND) {
			
			return className.exists (id);
			
		}
		
		#end
		
		return true;
		
	}
	
	
	public override function loadBitmapData (id:String, handler:BitmapData -> Void):Void {
		
		#if pixi
		
		handler (getBitmapData (id));
		
		#elseif (flash || js)
		
		if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event:Event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getBitmapData (id));
			
		}
		
		#else
		
		handler (getBitmapData (id));
		
		#end
		
	}
	
	
	public override function loadBytes (id:String, handler:ByteArray -> Void):Void {
		
		#if pixi
		
		handler (getBytes (id));
		
		#elseif (flash || js)
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bytes = new ByteArray ();
				bytes.writeUTFBytes (event.currentTarget.data);
				bytes.position = 0;
				
				handler (bytes);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getBytes (id));
			
		}
		
		#else
		
		handler (getBytes (id));
		
		#end
		
	}
	
	
	public override function loadFont (id:String, handler:Font -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getFont (id));
			
		//}
		
		#else
		
		handler (getFont (id));
		
		#end
		
	}
	
	
	public override function loadMusic (id:String, handler:Sound -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getMusic (id));
			
		//}
		
		#else
		
		handler (getMusic (id));
		
		#end
		
	}
	
	
	public override function loadSound (id:String, handler:Sound -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getSound (id));
			
		//}
		
		#else
		
		handler (getSound (id));
		
		#end
		
	}
	
	
	public override function loadText (id:String, handler:String -> Void):Void {
		
		#if js
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				handler (event.currentTarget.data);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getText (id));
			
		}
		
		#else
		
		var callback = function (bytes:ByteArray):Void {
			
			if (bytes == null) {
				
				handler (null);
				
			} else {
				
				handler (bytes.readUTFBytes (bytes.length));
				
			}
			
		}
		
		loadBytes (id, callback);
		
		#end
		
	}
	
	
}


#if pixi
#elseif flash

@:keep class __ASSET__assets_addbutton_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_background_jpg extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_background2_jpg extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_components_fxg extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_helveticaneue_eot extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_helveticaneue_svg extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_helveticaneue_ttf extends flash.text.Font { }
@:keep class __ASSET__assets_helveticaneue_woff extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_helveticaneuelight_eot extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_helveticaneuelight_svg extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_helveticaneuelight_ttf extends flash.text.Font { }
@:keep class __ASSET__assets_helveticaneuelight_woff extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_tagbody_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_tagbody_svg extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_tagcue_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_tagcue_svg extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_tagtip_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_tagtip_svg extends flash.utils.ByteArray { }
@:keep class __ASSET__assets_test_svg extends flash.utils.ByteArray { }


#elseif html5







@:keep class __ASSET__assets_helveticaneue_ttf extends flash.text.Font { #if (!openfl_html5_dom) public function new () { super (); fontName = "assets/helveticaneue.ttf"; } #end }



@:keep class __ASSET__assets_helveticaneuelight_ttf extends flash.text.Font { #if (!openfl_html5_dom) public function new () { super (); fontName = "assets/HelveticaNeueLight.ttf"; } #end }










#elseif (windows || mac || linux)


@:bitmap("Assets/addButton.png") class __ASSET__assets_addbutton_png extends flash.display.BitmapData {}
@:bitmap("Assets/background.jpg") class __ASSET__assets_background_jpg extends flash.display.BitmapData {}
@:bitmap("Assets/background2.jpg") class __ASSET__assets_background2_jpg extends flash.display.BitmapData {}
@:file("Assets/Components.fxg") class __ASSET__assets_components_fxg extends flash.utils.ByteArray {}
@:file("Assets/helveticaneue.eot") class __ASSET__assets_helveticaneue_eot extends flash.utils.ByteArray {}
@:file("Assets/helveticaneue.svg") class __ASSET__assets_helveticaneue_svg extends flash.utils.ByteArray {}
@:font("Assets/helveticaneue.ttf") class __ASSET__assets_helveticaneue_ttf extends flash.text.Font {}
@:file("Assets/helveticaneue.woff") class __ASSET__assets_helveticaneue_woff extends flash.utils.ByteArray {}
@:file("Assets/HelveticaNeueLight.eot") class __ASSET__assets_helveticaneuelight_eot extends flash.utils.ByteArray {}
@:file("Assets/HelveticaNeueLight.svg") class __ASSET__assets_helveticaneuelight_svg extends flash.utils.ByteArray {}
@:font("Assets/HelveticaNeueLight.ttf") class __ASSET__assets_helveticaneuelight_ttf extends flash.text.Font {}
@:file("Assets/HelveticaNeueLight.woff") class __ASSET__assets_helveticaneuelight_woff extends flash.utils.ByteArray {}
@:bitmap("Assets/tagBody.png") class __ASSET__assets_tagbody_png extends flash.display.BitmapData {}
@:file("Assets/tagBody.svg") class __ASSET__assets_tagbody_svg extends flash.utils.ByteArray {}
@:bitmap("Assets/tagCue.png") class __ASSET__assets_tagcue_png extends flash.display.BitmapData {}
@:file("Assets/tagCue.svg") class __ASSET__assets_tagcue_svg extends flash.utils.ByteArray {}
@:bitmap("Assets/tagTip.png") class __ASSET__assets_tagtip_png extends flash.display.BitmapData {}
@:file("Assets/tagTip.svg") class __ASSET__assets_tagtip_svg extends flash.utils.ByteArray {}
@:file("Assets/test.svg") class __ASSET__assets_test_svg extends flash.utils.ByteArray {}


#end
