package;

import flash.display.Sprite;
import ui.UiController;
import TagController;




class Main extends Sprite {
	

	var uiController : UiController;
	//var tagController : TagController;
	
	public function new ()
	{
		super();
		
		initTagController();
		initUi();

	}

	private function initUi()
	{
		
		uiController = new UiController();	
		addChild(uiController);
		//uiController.tagController = tagController;
		uiController.init();
	}

	private function initTagController()
	{
		//tagController = new TagController();
		TagController.init();
	}
}