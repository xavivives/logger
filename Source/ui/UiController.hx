package ui;


import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.display.Stage;
import openfl.Assets;
import flash.events.MouseEvent;
import flash.events.Event;
import TagController;
import flash.system.Capabilities;
import ui.BaseButton;
import ui.AssetFactory;
import ui.UiTag;
import motion.Actuate;


class UiController extends Sprite {
	
	//public var tagController : TagController;
	var assetFactory : AssetFactory;
	var prevMouseY : Float;
	var addButton : BaseButton;
	var addChildButton : BaseButton;
	var numberButton : BaseButton;
	var clockButton : BaseButton;
	var deleteButton : BaseButton;
	var tagButton : BaseButton;
	var background : Sprite;
	var uiTags:Array<ui.UiTag>;
	var tags:Array<SimpleTag>;
	var streamStage:Sprite;
	var editStreamStage:Sprite;
	var editBackground:Sprite;
	var hudStage:Sprite;
	var marginTop : Float;
	var marginLeft : Float;
	var marginBottom : Float;
	var marginTag:Float;
	var marginButton:Float;
	var maxTagWidth : Float;
	var tagHeight : Float;
	var tagIndent : Float;
	var scrollPos : Float;
	var isDragging:Bool;
	var editionArea:Sprite;
	var bottomArea:Sprite;
	var isAutomaticallyScrolling:Bool;
	var isEditing:Bool;
	var editingTag:UiTag;

	var line : flash.display.Shape;

	var t0 : Float;
	var y0 : Float;
	var v0 : Float;
	var v2 : Float; //to avarage v and make it smooth
	var v3 : Float;
	var tm : Float; //time of last recorded move event
	var  a : Float;
	var vs : Float; //max v to stop;
	var af : Float; //friction acceleation;


	var s : Stage;
	var frameRate: flash.text.TextField;
	var elapsedTime:Float;

	public function new ()
	{
		super ();
	}

	public function init()
	{
		t0 = flash.Lib.getTimer();
		y0 = 0;
		v0 = 0;
		a = 0;
		vs = 0.1;
		af = 0.005;
		
		s = flash.Lib.current.stage;

		initAssetFactory();

		marginTop = 30 * AssetFactory.getScale();
		marginBottom = 100 * AssetFactory.getScale();
		marginLeft = 20 * AssetFactory.getScale();
		marginTag = 10 * AssetFactory.getScale();
		marginButton = 27 * AssetFactory.getScale();
		maxTagWidth = 130 * AssetFactory.getScale();
		tagHeight = 14 * AssetFactory.getScale();
		tagIndent = 20 * AssetFactory.getScale();

		initBackground();
		initStreamStage();
		initButtons();
		initEditButtons();
		initLine();
		initEditionArea();
		initBottomArea();

		uiTags = new Array <UiTag> ();

		s.addEventListener (MouseEvent.MOUSE_DOWN, onMouseDown);
		s.addEventListener (Event.ENTER_FRAME, onEnterFrame);
		s.addEventListener (MouseEvent.MOUSE_WHEEL, onScroll);
		s.addEventListener (Event.RESIZE, onResize);

		createUiTags();
		layout();
		scrollTo(getStreamPosBottom());
	}

	function createUiTags()
	{
		function create(tag:SimpleTag, parent:UiTag)
		{
			var uiTag = createUiTag(parent);
			uiTag.setTag(tag);
			if(parent !=null)
				parent.addChildTag(uiTag); 
			uiTag.resize(false);
			uiTag.enableEdition(false);
			if(parent==null)
				uiTags.push(uiTag);
			streamStage.addChild(uiTag);
			var subtags:Array<SimpleTag> = uiTag.getTag().subTags;
			for(tag in subtags)
				create(tag,uiTag);
		}

		tags = TagController.getAll();

		for(tag in tags)
			create(tag, null);
	}


	function addUiTag(uiTag:UiTag)
	{
		uiTags.push(uiTag);
	}

	private function layout()
	{
		var currentHeight = marginTop;
		var currentLevel = 0;
		
		function layoutSubtags(uiTags: Array<UiTag>, level:Int)
		{
			for( i in 0...uiTags.length)
			{
				uiTags[i].y = currentHeight + 1*AssetFactory.getScale();
				uiTags[i].x = marginLeft + level*tagIndent;
				currentHeight =  uiTags[i].y  + uiTags[i].getHeight();
				layoutSubtags(uiTags[i].getChildren(),level+1);
			}
		}

		for( i in 0...uiTags.length)
		{
			uiTags[i].y = currentHeight + marginTag;
			uiTags[i].x = marginLeft ;
			currentHeight =  uiTags[i].y + uiTags[i].getHeight();
			layoutSubtags(uiTags[i].getChildren(),currentLevel +1);
		}

		if(isEditing)
			editionArea.y =  editingTag.y - marginTop;
		bottomArea.y = currentHeight;
	}

	private function initStreamStage()
	{
		streamStage = new Sprite();
		streamStage.graphics.beginFill(0xff00000,0.0);
		streamStage.graphics.drawRect(0,0,s.stageWidth,10);
		streamStage.graphics.endFill();
		streamStage.x = 0;
		editBackground = new Sprite();
		editBackground.mouseEnabled = editBackground.mouseChildren = false;
		editBackground.graphics.beginFill(0x000000,1);
		editBackground.graphics.drawRect(0,0,s.stageWidth,s.stageHeight);
		editBackground.graphics.endFill();
		editBackground.alpha = 0;

		editStreamStage = new Sprite();
		editStreamStage.visible = false;
		hudStage = new Sprite();

		frameRate = new flash.text.TextField();
		hudStage.addChild(frameRate);
		frameRate.text = "Hello!";
		frameRate.defaultTextFormat = new flash.text.TextFormat ("assets/HelveticaNeueLight.ttf", 12*AssetFactory.getScale(), 0xffffff);
		frameRate.x = s.stageWidth - 20 * AssetFactory.getScale();
		elapsedTime = 0;

		addChild(streamStage);
		addChild(editBackground);
		addChild(editStreamStage);
		addChild(hudStage);
	}

	private function initBottomArea()
	{
		bottomArea = new Sprite();
		streamStage.addChild(bottomArea);
		bottomArea.graphics.beginFill(0xf000, 0.0);
		bottomArea.graphics.drawRect(0,0,s.stageWidth, marginBottom);
		bottomArea.graphics.endFill();
	}

	private function initAssetFactory()
	{
		AssetFactory.init();
	}

	private function initBackground()
	{
		background = new Sprite();
		var image = AssetFactory.getAsset("assets/background2.jpg");
		image.y=-100*AssetFactory.getScale();
		background.addChild(image);
		addChild (background);
		resizeBackground();
	}

	private function resizeBackground()
	{
		background.width = s.stageWidth;
		background.scaleY = background.scaleX;
	}
	private function onMouseDown (event:MouseEvent):Void {

		s.addEventListener (MouseEvent.MOUSE_MOVE, onMouseMove);
		s.addEventListener (MouseEvent.MOUSE_UP, onMouseUp);
		isDragging = true;
		prevMouseY = s.mouseY;	
	}
	
	private function onMouseMove (e):Void {
		t0 = flash.Lib.getTimer();
		var mouseDelta = s.mouseY - prevMouseY;
		var v1 = mouseDelta / (t0 - tm);

		if(v1 > 15)
			v1 = 15;
		if(v1 < -15)
			v1 = -15;

		v0 = (v1 + v2 +v3) / 3;
		v3 = v2;
		v2 = v1;

		scrollPos = scrollPos + mouseDelta;
		y0 = scrollPos;

		prevMouseY = s.mouseY;
		tm = flash.Lib.getTimer();
		if(mouseDelta<5 && mouseDelta > -5)
		{
			v0 = 0;
			a = 0;
		}
		
	}

	private function onMouseUp (event:MouseEvent):Void
	{
		s.removeEventListener (MouseEvent.MOUSE_MOVE, onMouseMove);
		s.removeEventListener (MouseEvent.MOUSE_UP, onMouseUp);
		isDragging = false;
	}

	private function onScroll (event:MouseEvent):Void
	{
		restoreMovement();
		v0 = event.delta *2;
	}

	private function onResize (event):Void
	{
		resizeBackground();
	}

	private function restoreMovement()
	{
		t0 = flash.Lib.getTimer();
		y0 = scrollPos;
		v0 = 0;
		a = 0;
	}

	private function onEnterFrame (event:Event):Void
	{
		if(!isDragging && !isAutomaticallyScrolling &&!isEditing)
		{
			if(!scrollPosIsOutOfBounds())
			{
				
				var t = (flash.Lib.getTimer() - t0);
				var v = a * t + v0;

				if(v < vs && v > -vs)
				{
					restoreMovement();
				}
				else
				{
					if(v>0)
						a = -af;
					else
						a = af;
				}

				scrollPos = y0+ v0*t + a * Math.pow(t,2)/2;
			}
				
		}

		if(getStreamPosBottom()>scrollPos)
		{
			
			var newScrollPos  =  getStreamPosBottom() ;
			scrollPos = scrollPos + (newScrollPos - scrollPos) * 0.2;
			//scrollPos = newScrollPos;


		}
		else if (scrollPos > 0)
		{
			var newScrollPos  =  0;
			scrollPos = scrollPos + (newScrollPos - scrollPos) * 0.2;
			//scrollPos = newScrollPos;
		}

		scrollPos = Math.round(scrollPos*1000)/1000;
			
		background.y = scrollPos*0.3;
		streamStage.y = scrollPos;
		editStreamStage.y = scrollPos;

		//layout();
		showFrameRate();
	}

	function showFrameRate()
	{
		var fr =Math.round( 1/(flash.Lib.getTimer() - elapsedTime)*1000);
		if(elapsedTime%2 == 0)
				frameRate.text = Std.string(fr);
		elapsedTime = flash.Lib.getTimer();
	}

	private function scrollPosIsOutOfBounds():Bool
	{
		if(getStreamPosBottom()>scrollPos)
			return true;
 
		if(scrollPos > 0)
			return true;
		
		return false;
	}

	private function initButtons()
	{
		addButton = new BaseButton(AssetFactory.getAsset("assets/addButton.svg"), onAddButton);
		hudStage.addChild(addButton);
		addButton.x = marginLeft - addButton.width*0.5;
		addButton.y = s.stageHeight- 50*AssetFactory.getScale();

		deleteButton = new BaseButton(AssetFactory.getAsset("assets/deleteButton.svg"), onDeleteButton);
		hudStage.addChild(deleteButton);
		deleteButton.x = marginLeft - deleteButton.width*0.5 + marginButton*4;
		deleteButton.y = s.stageHeight- 50*AssetFactory.getScale();
	}

	private function initEditButtons()
	{
		tagButton = new BaseButton(AssetFactory.getAsset("assets/tagButton.svg"), onTagButton, true);
		editStreamStage.addChild(tagButton);
		tagButton.enable(true);

		numberButton = new BaseButton(AssetFactory.getAsset("assets/numberButton.svg"), onNumberButton, true);
		editStreamStage.addChild(numberButton);
		numberButton.enable(true);

		clockButton = new BaseButton(AssetFactory.getAsset("assets/clockButton.svg"), onClockButton, true);
		editStreamStage.addChild(clockButton);
		clockButton.enable(false);
		clockButton.setState(false);

		addChildButton = new BaseButton(AssetFactory.getAsset("assets/addButton.svg"), onClockButton, false);
		editStreamStage.addChild(addChildButton);
		addChildButton.enable(true);
	}

	private function enableEditButtonsForTag(uiTag:UiTag)
	{
		tagButton.x = marginLeft + maxTagWidth + marginButton*1;
		tagButton.y = uiTag.y;	

		numberButton.x = marginLeft + maxTagWidth + marginButton*2;
		numberButton.y = uiTag.y;

		clockButton.x = marginLeft + maxTagWidth + marginButton*3;
		clockButton.y = uiTag.y;

		addChildButton.x = marginLeft;
		addChildButton.y = uiTag.y + marginTag;
	}


	public function onAddButton()
	{
		if(isEditing)
			exitEditionModeForTag();

		var uiTag= createUiTag();
		addUiTag(uiTag);
		enterEditionModeForTag(uiTag);
		layout();
		uiTag.enableEdition(true);
		scrollTo(getStreamPosBottom());
	}

	public function onTagButton()
	{
		tagButton.setState(true);
		numberButton.setState(false);
		clockButton.setState(false);
		editingTag.setType(Simple);
	}

	public function onNumberButton()
	{
		numberButton.setState(true);
		tagButton.setState(false);
		clockButton.setState(false);
		editingTag.setType(Numeric);
	}

	public function onClockButton()
	{
		clockButton.setState(true);
		numberButton.setState(false);
		tagButton.setState(false);
		editingTag.setType(TimeRange);
	}

	public function onDeleteButton()
	{
		destroyAll();
	}

	public function destroyAll()
	{
		while (uiTags.length>0)
		{	
			TagController.deleteTag(uiTags[0].getTag());
			destroyUiTag(uiTags[0]);
			uiTags.remove(uiTags[0]);
		}

		TagController.save();
	}

	function destroyUiTag(uiTag:UiTag)
	{
		if(streamStage.contains(uiTag))
			streamStage.removeChild(uiTag);
		if(editStreamStage.contains(uiTag))
			editStreamStage.removeChild(uiTag);

		while (uiTag.getChildren().length>0)
		{
			destroyUiTag(uiTag.getChildren()[0]);
			//uiTag.getChildren().remove(uiTag.getChildren()[0]);		
			
		}
		if(uiTag.getParent()!=null)
			uiTag.getParent().getChildren().remove(uiTag);

	}

	public function createUiTag(?parent:UiTag = null):UiTag
	{
		var uiTag = new UiTag();
		uiTag.init(parent,onTagEditionFinished, onTagTypeChanged, onTagClicked);
		return uiTag;	
	}

	public function onTagClicked(clickedUiTag: UiTag)
	{
		var newUiTag = createUiTag(clickedUiTag);
		clickedUiTag.addChildTag(newUiTag);
		enterEditionModeForTag(newUiTag);
		newUiTag.enableEdition(true);
		layout();
		scrollTo(getStreamPosForTag(editingTag));
	}

	public function onTagEditionFinished(uiTag:UiTag, created:Bool)
	{
		exitEditionModeForTag(uiTag);
		
		if(created)
		{
			TagController.save();
		}
		else
		{
			destroyUiTag(uiTag);
		}
		editingTag = null;
		layout();
	}

	public function onTagTypeChanged(uiTag:UiTag, newType:TagType)
	{
		if(newType == Numeric)
		{
			tagButton.setState(false);
			numberButton.setState(true);
			clockButton.setState(false);
		}
		else
		{
			tagButton.setState(true);
			numberButton.setState(false);
			clockButton.setState(false);
		}
	}


	function initLine()
	{
		line = new flash.display.Shape();
		line.x = marginLeft*2.6;
		line.graphics.lineStyle(0.2,0xffffff,1);
		line.graphics.moveTo(0,0);
		line.graphics.lineTo(0, s.stageHeight - marginBottom);
		hudStage.addChild(line);	
	}

	function initEditionArea()
	{
		editionArea = new Sprite();
		streamStage.addChild(editionArea);
	}

	function enterEditionModeForTag(tagToEdit:UiTag)
	{
		isEditing = true;
		editingTag = tagToEdit;

		addButton.enable(false);
		deleteButton.enable(false);
		
		editStreamStage.visible = true;
		enableEditButtonsForTag(editingTag);

		for(uiTag in uiTags)
			uiTag.setAlpha(0.1);

		if(editingTag != null)
			editingTag.setAlpha(1);

		editStreamStage.addChild(editingTag);

		//Actuate.tween (editBackground, 0.4, { alpha: 0.4 });
		editBackground.alpha = 0.4;
		editBackground.visible = true;

		editionArea.graphics.clear();
		editionArea.graphics.beginFill(0xff0000, 0.0);
		editionArea.graphics.drawRect(0,0,s.stageWidth, s.stageHeight);
	}

	function exitEditionModeForTag(?editedTag:UiTag = null)
	{
		isEditing = false;

		if(editedTag != null)
		{
			streamStage.addChild(editedTag);
		}

		editStreamStage.visible = false;

		addButton.enable(true);
		deleteButton.enable(true);

		//Actuate.tween (editBackground, 0.4, { alpha: 0.0 });
		editBackground.visible = false;
		editionArea.graphics.clear();

		for(uiTag in uiTags)
		{
			uiTag.setAlpha(1);

		}
	}

	function getStreamPosForTag(uiTag:UiTag):Float
	{
		return   marginTop - uiTag.y;
	}

	inline function getStreamPosBottom():Float
	{
		return s.stageHeight -streamStage.height;
	}

	inline function getStreamPosTop():Float
	{
		return 0;
	}

	function scrollTo(newPos:Float)
	{
		isAutomaticallyScrolling = true;
		Actuate.tween (this, 0.7, { scrollPos: newPos }).onComplete(scrollCompleted,null);
	}

	function scrollCompleted(_)
	{
		restoreMovement();
		isAutomaticallyScrolling = false;
	}
}