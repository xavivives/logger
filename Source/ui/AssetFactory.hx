package ui;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.display.Stage;
import flash.display.Shape;
import openfl.Assets;
import flash.events.MouseEvent;
import flash.events.Event;
import ui.BaseButton;
import TagController;
import flash.system.Capabilities;
import format.SVG;

 class AssetFactory {

	static var scale:Float;

	public function new()
	{
	}

	static public function init()
	{
		#if (html5 || flash)
			scale = 1;
		#else
		scale = flash.system.Capabilities.screenDPI / 126;
		#end
		//scale = 326/126; //iphone 4 resolution
	}

	static public function getAsset(path:String) : Sprite
	{
		if(path.toLowerCase().indexOf(".svg") != -1)
			return(getSvg(path));
		else
			return(getBitmap(path));
	}


	static function getSvg(path:String, ?scale:Bool=true):Sprite
	{
		var sprite = new Sprite();

		var shape = new Shape();
		sprite.addChild(shape);//to put the svg already scaled
		var svgFile = Assets.getText(path);
        var svg = new SVG(svgFile);
        svg.render(shape.graphics, 0, 0);
        if(scale)
        	shape.scaleX = shape.scaleY = getScale() * 0.55; // hack 
    	//sprite.cacheAsBitmap = true;

    	return sprite;
	}

	static function getBitmap(path:String):Sprite
	{
		var bitmap = new Bitmap (Assets.getBitmapData (path));
		var sprite = new Sprite();
		bitmap.scaleY = bitmap.scaleX = getScale() /4; //we assume all restarized assets are 4x its normal size
		bitmap.smoothing = true;
		sprite.addChild (bitmap);
		return sprite;
	}

	static public function getScale():Float
	{
		return scale;
	}
}