package ui;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.MouseEvent;
import openfl.Assets;
import ui.AssetFactory;
import flash.filters.DropShadowFilter;

class BaseButton extends Sprite
{
	var asset: Sprite;
	var hitarea : Sprite;
	var isStateButton : Bool;
	var isEnabled : Bool;
	var selected:Bool;
	var shadowFilter : DropShadowFilter;

	var onClickCallback : Void ->Void;

	public function new(_asset: Sprite, onClickCallback:Void->Void, ?_isStateButton:Bool = false)
	{
		super();
		asset = _asset;
		addChild (asset);

		this.onClickCallback = onClickCallback;
		initHitArea();
		enable(true);

		isStateButton = _isStateButton;

		if(isStateButton)
			setState(true);


		shadowFilter = new DropShadowFilter();
		shadowFilter.distance = 2;
		shadowFilter.color = 0x333333;
		shadowFilter.blurX = 3;
		shadowFilter.blurY = 3;
		shadowFilter.quality = 1;
		shadowFilter.inner = true;
		shadowFilter.hideObject = true;
		shadowFilter.angle = 90;
		shadowFilter.strength = 0.01;
	}

	public function enable(enable:Bool)
	{
		isEnabled = enable;

		if(enable)
		{
			hitarea.addEventListener(MouseEvent.CLICK, onClick);
			hitarea.addEventListener(MouseEvent.MOUSE_OVER, onOver);
			hitarea.addEventListener(MouseEvent.MOUSE_OUT, onOut);
		}
		else
		{
			hitarea.removeEventListener(MouseEvent.CLICK, onClick);
			hitarea.removeEventListener(MouseEvent.MOUSE_OVER, onOver);
			hitarea.removeEventListener(MouseEvent.MOUSE_OUT, onOut);
		}
		
		setAlpha();
	}

	function initHitArea()
	{
		hitarea = new Sprite();
		hitarea.graphics.beginFill(0xFF0000, 0.0); 
		hitarea.graphics.drawRect(0, 0, asset.width, asset.height); 
		addChild(hitarea);		
	}

	function onOver(_)
	{
	}

	function onOut(_)
	{

	}

	function onClick(_)
	{
		onClickCallback();
	}

	public function setState(_selected:Bool)
	{
		selected = _selected;
		
		if(selected)
			asset.transform.colorTransform = new flash.geom.ColorTransform();

		else
			asset.transform.colorTransform = new flash.geom.ColorTransform( 0 ,0 ,0 ,1 ,20 ,20 ,20 ,0 );

		setAlpha();
	}

	public function isSelected():Bool
	{
		return selected;
	}

	public function setAlpha()
	{
		if(isEnabled)
			asset.alpha = 0.5;
		else
			asset.alpha = 0.2;
	}


}