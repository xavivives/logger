package ui;

import ui.SuperTextField;
import flash.display.Sprite;
import flash.text.TextField;

import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.TextFieldAutoSize;
import flash.events.TextEvent;
import openfl.Assets;
import ui.AssetFactory;
import format.SVG;
import flash.text.Font;
import flash.display.Shape;
import flash.events.KeyboardEvent;
import flash.events.FocusEvent;
import flash.events.Event;
import motion.Actuate;
import TagController;

class UiTag extends Sprite
{
	var background: Sprite;
	var shape:Shape;
	var nameLabel : TextField;
	var nameGhostLabel : TextField;
	var valueGhostLabel : TextField;
	var timeLabel : TextField;
	var valueLabel : TextField;
	var triangleSide:Float;
	var withTip : Bool;
	var defaultLength:Float; //nameLabel.textWidth + nameValue.textWidth
	var nameLabelLength:Float;
	var valueLabelLength:Float;
	var timeLabelLength:Float;
	var totalMaxLength:Float;
	var tag:SimpleTag;
	var nameGhostText:String;
	var valueGhostText:String;
	var nameGhostShape:Sprite;
	var valueGhostShape:Sprite;
	var created: Bool;
	var onEditionFinished:UiTag -> Bool -> Void;
	var onTypeChanged:UiTag -> TagType -> Void;
	var onClicked: UiTag -> Void;
	var type: TagType;
	var content:Sprite;
	var isEditing : Bool;
	var all:Sprite;
	var bitmap : flash.display.Bitmap;
	var parentUiTag : UiTag;
	var children : Array <UiTag>;
	var suggestionLayer:Sprite;
	var color : Int;


	public function new()
	{
		super();			
	}

	public function init( _parentUiTag : UiTag, _onEditionFinished:UiTag -> Bool->Void, _onTypeChanged: UiTag -> TagType -> Void,  _onClicked: UiTag  -> Void)
	{	
		//addChild(bitmap);

		triangleSide = 12 * AssetFactory.getScale();
		nameLabelLength = 65 * AssetFactory.getScale();
		valueLabelLength = 35 * AssetFactory.getScale();
		defaultLength = nameLabelLength + valueLabelLength;
		totalMaxLength = 100*AssetFactory.getScale();
		timeLabelLength = 35 * AssetFactory.getScale();

		nameGhostText = "tag name";
		valueGhostText = "value";
		created = false;
		type = Simple;
		children = new Array <UiTag>();
		color = 0xffffff;
		parentUiTag = _parentUiTag;
		onEditionFinished = _onEditionFinished;
		onTypeChanged = _onTypeChanged;
		onClicked = _onClicked;

		initAll();
		initBackground();
		initContent();
		initGhostShapes();

		if(parentUiTag == null)
			withTip = true;
		else
			color = parentUiTag.getColor();

			initTimeLabel();

		initNameTextField();
		initValueLabel();
		initNameGhostField();
		initValueGhostField();
		
		layout();
		checkGhostText();
	}

	public function getTag():SimpleTag
	{
		return tag;
	}

	public function setTag(newTag:SimpleTag)
	{
		if(newTag!=null)
			tag = newTag;

		nameLabel.text = tag.name;

		if(withTip)
			timeLabel.text = timeStampToString();

		if(tag.type == "numeric")
			setNumericTag(TagController.simpleToNumeric(newTag));

		checkGhostText();
		setColor();
	}

	public function addChildTag(tag:UiTag)
	{
		children.push(tag);
	}

	public function getChildren():Array<UiTag>
	{
		return children;
	}

	function setNumericTag(tag:NumericTag)
	{
		setType(Numeric);
		valueLabel.text = Std.string(tag.value);
	}

	public function enableEdition(enable:Bool)
	{
		if(enable)
		{
			isEditing = true;
			nameLabel.selectable = true;
			nameLabel.type = TextFieldType.INPUT;
			valueLabel.selectable = true;
			valueLabel.type = TextFieldType.INPUT;
			stage.focus = nameLabel;
			convertToVector();
		}
		else
		{
			isEditing = false;
			clearFieldsBackground();
			nameLabel.type = TextFieldType.DYNAMIC;
			nameLabel.selectable = false;
			valueLabel.type = TextFieldType.DYNAMIC;
			valueLabel.selectable = false;
			convertToBitmap();
		}
	}

	public function resize(animate:Bool)
	{
		layout();
	}

	public function  setType(newType: TagType)
	{
		type = newType;
		layout();
		if(isEditing)
			if(stage== null)
				trace("tag is not added tot stage");
			else
				stage.focus = valueLabel;

	}

	public function setWithTip(hasTip:Bool)
	{
		withTip = hasTip;
		layout();
	}

	public function setAlpha(alpha:Float)
	{
		this.alpha = alpha;
		for(child in getChildren())
			child.setAlpha(alpha);
	}

	public function getParent():UiTag
	{
		return parentUiTag;
	}

	public function getHeight():Float
	{
		return background.height;
	}
	
	function layout()
	{
		content.x = timeLabelLength + triangleSide*1.5;
		content.y = 2*AssetFactory.getScale();

		nameLabel.height =  (Math.sqrt(3) * triangleSide);//nameLabel.textHeight;		
		nameLabel.width  = nameLabel.textWidth + 3 * AssetFactory.getScale() ;
		
		if(nameLabel.text == "")
			nameLabel.width = nameLabelLength;
		valueLabel.visible = false;

			valueLabel.height = nameLabel.height;
		if(type != Simple)
		{
			valueLabel.visible = true;

			valueLabel.width = valueLabel.textWidth + 3 * AssetFactory.getScale();

			if(valueLabel.text == "")
				valueLabel.width = valueLabelLength;

			if(isEditing)
			{
				if(nameLabel.width > nameLabelLength)
				nameLabel.width = nameLabelLength;
				
				if(valueLabel.width > valueLabelLength)
					valueLabel.width = valueLabelLength;
				if(valueLabel.x + valueLabel.width > totalMaxLength)
					nameLabel.width = totalMaxLength - valueLabel.width - 3 * AssetFactory.getScale();
			}

			valueLabel.x = nameLabel.width + triangleSide ;
			valueLabel.y = nameLabel.y;

		}

		nameGhostLabel.width = nameLabel.width;
		nameGhostLabel.height = nameLabel.height;
		nameGhostLabel.x = nameLabel.x;
		nameGhostLabel.y = nameLabel.y;

		valueGhostLabel.width = valueLabel.width;
		valueGhostLabel.height = valueLabel.height;
		valueGhostLabel.x = valueLabel.x;
		valueGhostLabel.y = valueLabel.y;

		if(type ==Simple)
			drawTag(nameLabel.width );
		else
			drawTag(valueLabel.x + valueLabel.width);

		clearFieldsBackground();
		if(isEditing)
		{
			drawNameBackground();
			if(type!= Simple)
				drawValueBackground();
		}

	}

	function initAll()
	{
		all = new Sprite();
		addChild(all);
		bitmap = new flash.display.Bitmap();

	}

	function initContent()
	{
		content = new Sprite();
		all.addChild(content);
		suggestionLayer = new Sprite();
		addChild(suggestionLayer);
	}

	function initNameTextField()
	{
		nameLabel = new TextField();
		nameLabel.background = false;
		nameLabel.selectable = true;
		nameLabel.type = TextFieldType.INPUT;
		nameLabel.defaultTextFormat = new TextFormat ("assets/HelveticaNeueLight.ttf", triangleSide, 0xffffff);
		nameLabel.embedFonts = true;
		content.addChild(nameLabel);

		nameLabel.addEventListener(FocusEvent.FOCUS_IN, onNameLabelFocusIn);
		nameLabel.addEventListener(FocusEvent.FOCUS_OUT, onNameLabelFocusOut);
		nameLabel.addEventListener(KeyboardEvent.KEY_DOWN, onNameLabelKeyDown);
		nameLabel.addEventListener(Event.CHANGE, onNameLabelChange);
	}

	function initValueLabel()
	{
		valueLabel = new TextField();
		valueLabel.background = false;
		valueLabel.selectable = true;
		valueLabel.type = TextFieldType.INPUT;
		valueLabel.defaultTextFormat = new TextFormat ("assets/HelveticaNeueLight.ttf", triangleSide*1, 0xffffff);
		
		content.addChild(valueLabel);

		valueLabel.addEventListener(KeyboardEvent.KEY_DOWN, onValueLabelKeyDown);
		valueLabel.addEventListener(Event.CHANGE, onValueLabelChange);
	}

	function initBackground()
	{
		background = new Sprite();
		all.addChild(background);
		shape = new Shape();
		background.addChild(shape);
		background.mouseChildren = background.mouseEnabled = false;
		background.x = timeLabelLength;
		drawTag(defaultLength);
	}

	function initNameGhostField()
	{
		nameGhostLabel = new TextField();
		nameGhostLabel.background = false;
		nameGhostLabel.selectable = false;
		nameGhostLabel.mouseEnabled = false;
		nameGhostLabel.type = TextFieldType.DYNAMIC;
		nameGhostLabel.defaultTextFormat = new TextFormat ("assets/HelveticaNeueLight.ttf", triangleSide, 0xffffff);		
		nameGhostLabel.alpha = 0.5;
		nameGhostLabel.text = nameGhostText;
		content.addChild(nameGhostLabel);
	}

	function initValueGhostField()
	{
		valueGhostLabel = new TextField();
		valueGhostLabel.background = false;
		valueGhostLabel.selectable = false;
		valueGhostLabel.mouseEnabled = false;
		valueGhostLabel.type = TextFieldType.DYNAMIC;
		valueGhostLabel.defaultTextFormat = new TextFormat ("assets/HelveticaNeueLight.ttf", triangleSide, 0xffffff);		
		valueGhostLabel.alpha = 0.5;
		valueGhostLabel.text = valueGhostText;
		content.addChild(valueGhostLabel);
	}

	
	function initTimeLabel()
	{
		timeLabel = new TextField();
		timeLabel.background = false;
		timeLabel.selectable = false;
		timeLabel.type = TextFieldType.DYNAMIC;

		timeLabel.defaultTextFormat = new TextFormat ("assets/HelveticaNeueLight.ttf", triangleSide*0.9, 0xffffff);
		//if(withTip)
			all.addChild(timeLabel);

		timeLabel.width  = timeLabelLength ;
		timeLabel.height =  timeLabel.textHeight + 5 * AssetFactory.getScale() ;
		timeLabel.x = 0;//-timeLabelLength;//- timeLabel.textWidth  - 5 * AssetFactory.getScale();
		timeLabel.y = 0;//- timeLabel.textHeight * 0.5;
	}

	function timeStampToString():String
	{
		var date = Date.fromTime(tag.timeStamp);
		return DateTools.format(date, "%H:%M");
	}

	function checkGhostText()
	{
		if(nameLabel.text == "")
			nameGhostLabel.visible = true;
		else
			nameGhostLabel.visible = false;

		if(valueLabel.text == "")
			valueGhostLabel.visible = true;
		else
			valueGhostLabel.visible = false;

		if(type == Simple)
			valueGhostLabel.visible = false;
	}

	function initGhostShapes()
	{
		nameGhostShape = new Sprite();
		content.addChild (nameGhostShape);
		nameGhostShape.addEventListener(flash.events.MouseEvent.CLICK,function(e){
			stage.focus = nameLabel;
			nameLabel.setSelection(nameLabel.text.length,nameLabel.text.length);
			});

		valueGhostShape = new Sprite();
		content.addChild (valueGhostShape);
		valueGhostShape.addEventListener(flash.events.MouseEvent.CLICK,function(e){
			stage.focus = nameLabel;
			nameLabel.setSelection(nameLabel.text.length,nameLabel.text.length);
			});
	}

	function drawNameBackground()
	{
		nameGhostShape.graphics.beginFill(0x000000, 0.4);
		nameGhostShape.graphics.drawRoundRect(nameLabel.x-2*AssetFactory.getScale(), nameLabel.y+1*AssetFactory.getScale(), nameLabel.width+4*AssetFactory.getScale(), nameLabel.height - 5 *AssetFactory.getScale(), 4*AssetFactory.getScale(), 4*AssetFactory.getScale());
		nameGhostShape.graphics.endFill();
	}

	function drawValueBackground()
	{
		valueGhostShape.graphics.beginFill(0x000000, 0.4);
		valueGhostShape.graphics.drawRoundRect(valueLabel.x-2*AssetFactory.getScale(), valueLabel.y+1*AssetFactory.getScale(), valueLabel.width+4*AssetFactory.getScale(), valueLabel.height - 5 *AssetFactory.getScale(), 4*AssetFactory.getScale(), 4*AssetFactory.getScale());
		valueGhostShape.graphics.endFill();
	}
	function clearFieldsBackground()
	{
		nameGhostShape.graphics.clear();
		valueGhostShape.graphics.clear();
	}

	function drawTag (length:Float)
	{
		var w = triangleSide * 0.5; //half triangle width
		var h = Math.sqrt(3) * w; //triangle height
		//var l = w + unitsLengh*(w*2);
		var l = length + triangleSide * 2 ;//length of the center part

		shape.graphics.clear();
		shape.graphics.beginFill(color, 0.5);
		if(withTip)
		{
			shape.graphics.moveTo(0,0);
			shape.graphics.lineTo(w+w,0);	
			shape.graphics.lineTo(w+l,0);
			shape.graphics.lineTo(w+l+w,h);
			shape.graphics.lineTo(w+l,h+h);
			shape.graphics.lineTo(w+w,h+h);
			shape.graphics.lineTo(0,0);
		}
		else
		{
			shape.graphics.moveTo(0,h);
			shape.graphics.lineTo(w,0);	
			shape.graphics.lineTo(l,0);
			shape.graphics.lineTo(l+w,h);
			shape.graphics.lineTo(l,h+h);
			shape.graphics.lineTo(w,h+h);
			shape.graphics.lineTo(0,h);
		}

		shape.graphics.endFill();
	}

	private function onDone()
	{
		if(nameLabel.text == "")
			created = false;
		else
			created = true;

		if(created)
		{
			createTagFromInput();

			nameLabel.scrollH = 0;
			valueLabel.scrollH = 0;

			resize (true);		
		}
		stage.focus = stage;
		enableEdition(false);

		onEditionFinished(this,created);
	}

	function convertToBitmap()
	{
		var bounds = all.getBounds(this);
		var bitmapData = new flash.display.BitmapData(Math.round(all.width + all.x),Math.round(all.height));
		bitmapData.fillRect(new flash.geom.Rectangle(bounds.x,bounds.y,bounds.width,bounds.height),0x00000000);
		bitmapData.draw(all);
		 bitmap = new flash.display.Bitmap(bitmapData);
		addChild(bitmap);
		if(this.contains(all))
			removeChild(all);

		this.addEventListener(flash.events.MouseEvent.CLICK, onBitmapClick);
	}

	function convertToVector()
	{
		if(contains(bitmap))
			removeChild(bitmap);
		addChild(all);
	}

	function onBitmapClick(e)
	{
		onClicked(this);
	}

	function createTagFromInput()
	{
		var tag : SimpleTag;
		if(type == Simple)
			 tag = TagController.createNewTag(nameLabel.text, Date.now().getTime());
		else  
			tag = TagController.createNewNumericTag(nameLabel.text, Date.now().getTime(),Std.parseFloat(valueLabel.text),"units");
		
		if(getParent() == null)
			TagController.addTag(tag);
		else
			TagController.addSubTagToTag(tag, getParent().getTag());

		setTag(tag);
	}

	function setColor()
	{
		if(parentUiTag == null)
			//var topAncestor = TagController.getAncestors(parentUiTag.getTag())[0];
			color = TagController.getColor(nameLabel.text);
		else
		{
			color = parentUiTag.getColor();
		}

		layout();
	}

	public function getColor()
	{
		return color;
	}

	function onNameLabelFocusIn(e)
	{
		layout();
		showSuggestedTags();
	}

	function onNameLabelFocusOut(e)
	{
		//onEditionFinished(this,created);
	}

	function onNameLabelChange(e)
	{
		haxe.Timer.delay(onNameLabelChanged, 1);
	}

	function onNameLabelChanged()
	{
		cleanValueLabel();
		showSuggestedTags();
		setColor();
	}

	function onNameLabelKeyDown(e:KeyboardEvent)
	{
		if(e.keyCode == 13)//return
			if(type == Simple)
				onDone();
			else
			{
				stage.focus = valueLabel;
				nameLabel.scrollH = 0;
			}

		else if(e.keyCode == 32 || e.keyCode == 9) //space or tab
		{
			stage.focus = valueLabel;
			onTypeChanged(this,Numeric);
			setType(Numeric);
			
		}

	
	}

	function onValueLabelKeyDown(e:KeyboardEvent)
	{
		if(e.keyCode == 13)
		{
			if(valueLabel.text == "")
				setType(Simple);
			onDone();
		}
	}

	function onValueLabelChange(e)
	{
		haxe.Timer.delay(onValueLabelChanged, 1);
	}

	function onValueLabelChanged()
	{
		cleanValueLabel();
	}

	function cleanValueLabel()
	{
		valueLabel.text=getNumbers(valueLabel.text);
		checkGhostText();
	}

	function getAlfaNumericCharacters(string:String):String
	{
		var r:EReg = ~/^[A-Za-z0-9]*$/;

		var output = "";

		for(i in 0 ... string.length)
		{
			var char:String =string.charAt(i);
			if(r.match(char))
				output += char;
		}
		return (output);
	}

	function getNumbers(string:String):String
	{
		var r:EReg = ~/^[.,0-9]*$/;

		var output = "";

		for(i in 0 ... string.length)
		{
			var char:String =string.charAt(i);
			if(r.match(char))
				output += char;
		}
		output= output.split(",").join(".");
		return (output);
	}

	function removeCharacter(string:String, character:String):String
	{
		return string.split(character).join("");
	}

	function showSuggestedTags()
	{
		clearSuggestionTags();

		var parentTag:SimpleTag = null;
		if(getParent() != null)
			parentTag = getParent().getTag();
		var suggestedTags = TagController.getSubTagSuggestionsFor(parentTag,nameLabel.text);
		//var suggestedUiTags = Array<UiTag>;

		for(i in 0...suggestedTags.length)
		{
			var uiTag= new UiTag();
			uiTag.init(null, null, null, onSuggestionClicked);
			uiTag.setWithTip(false);
			uiTag.setTag(suggestedTags[i]);
			uiTag.enableEdition(false);
			uiTag.alpha = 0.5;
			uiTag.y = (i+1) * getHeight();
			suggestionLayer.addChild(uiTag);

		}
	}
	function clearSuggestionTags()
	{
		while (suggestionLayer.numChildren >0)
			suggestionLayer.removeChildAt(0);
	}

	function onSuggestionClicked(uiTag:UiTag)
	{
		var tag = uiTag.getTag();
		nameLabel.text = tag.name;
		
		if(tag.type == "numeric")
		{
			stage.focus = valueLabel;
			onTypeChanged(this,Numeric);
			setType(Numeric);
		}
		else
		{
			//onDone();
		}		
			nameLabel.scrollH = 0;
			clearSuggestionTags();
			checkGhostText();
	}
}