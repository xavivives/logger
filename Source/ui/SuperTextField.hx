package ui;


import flash.display.Sprite;
import flash.display.Shape;
import flash.text.TextField;

import flash.text.TextFieldType;
import flash.text.TextFormat;


class SuperTextField extends Sprite
{

	var textField : TextField;
	
	public function new(_textField : TextField)
	{
		super();
		textField  = _textField;
		addChild(textField);
		
	}

	public  function selectAll(start:Int, end:Int)
	{

		var lineMerics = textField.getLineMetrics(0);
		var w = lineMerics.width;
		var h = lineMerics.height;
		var x = lineMerics.x +2 ;
		var y =  2;
		
		graphics.beginFill(0xffffff, 0.5);
		graphics.moveTo(0,0);
		graphics.drawRect(x,y,w,h);
		//graphics.lineTo(w+w,0);	
		
		graphics.lineTo(0,0);
	}

}