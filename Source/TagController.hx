package;



class TagController  {

	static var tags : Array<SimpleTag>;
	static var colors:Array< Int>;
	static var tagsColors:Map<String,Int>;
	
	static var fileName= "/log.json";
	static var androidStorageDirectory = "/storage/sdcard0/Android/data/com.xavivives.logger";
	
	public function new ()
	{
	}

	static public function init()
	{
		tags = new Array<SimpleTag>();
		tagsColors = new Map<String,Int>();
		setColors();
		load();
	}

	static function setColors()
	{
		colors = new Array<Int>();
		colors.push(0xf00000);
		colors.push(0x00ff00);
		colors.push(0x0000ff);
		colors.push(0xffff00);
	}

	static public function load()
	{
		var jsonString = "";

		#if android
			if (sys.FileSystem.exists(androidStorageDirectory +fileName ))
				jsonString =sys.io.File.getContent(androidStorageDirectory+fileName);
		#end
		
		#if mac
			if (sys.FileSystem.exists(System.applicationStorageDirectory + fileName))
				jsonString =sys.io.File.getContent(System.applicationStorageDirectory+fileName);
		#end
		tags = haxe.Json.parse(jsonString);
		
	}

	static public function save()
	{

		var jsonString = haxe.Json.stringify(tags, null, " ");

		#if android
			if (!sys.FileSystem.exists(androidStorageDirectory +fileName ))
			{
				sys.FileSystem.createDirectory(androidStorageDirectory); 
				sys.io.File.write(androidStorageDirectory +fileName, false);
			}
			else
			{
				sys.io.File.saveContent(androidStorageDirectory + fileName, jsonString); 
			}
		#end
		
		#if mac
			if (!sys.FileSystem.exists(System.applicationStorageDirectory + fileName))
			{
				sys.FileSystem.createDirectory(System.applicationStorageDirectory); 
				sys.io.File.write(System.applicationStorageDirectory +fileName, false);
			}
			else
			{
				sys.io.File.saveContent(System.applicationStorageDirectory + fileName, jsonString); 
			}
		#end

	}

	static public function getAll():Array<SimpleTag>
	{
		return tags;
	}

	static public function addTag(tag:SimpleTag)
	{
		tags.push(tag);
	}

	static public function createNewTag(name:String, timeStamp:Float):SimpleTag
	{
		var guid = createGuid(name,timeStamp);

		var tag : SimpleTag = {
		  	name : name,
		  	timeStamp : timeStamp,
		  	subTags : new Array<SimpleTag>(),
		  	guid : guid,
		  	type : "simple",
		 }

		return tag;
	}

	static public function createNewNumericTag(name:String, timeStamp:Float, value:Float, valueType:String):NumericTag
	{
		var guid = createGuid(name,timeStamp);

		var tag : NumericTag = {
		  	name : name,
		  	timeStamp : timeStamp,
		  	subTags : new Array<SimpleTag>(),
		  	guid : guid,
		  	value: value,
		  	valueType : valueType,
		  	type : "numeric",
		 }

		return tag;
	}

	static public function deleteTag(tag:SimpleTag)
	{
		tags.remove(tag);
		
	}

	static public function setNameToTag(name : String, tag:SimpleTag)
	{
		tag.name = name;
	}

	static public function setTimeStampToTag(timeStamp:Int, tag:SimpleTag)
	{
		tag.timeStamp = timeStamp;
	}

	static public function addSubTagToTag(subTag : SimpleTag, tag:SimpleTag)
	{
		tag.subTags.push(subTag);
	}

	static public function addNumberToTag(value:Float, valueType:String, tag: NumericTag)
	{
		tag.value = value;
		tag.valueType = valueType;
	}

	static public function addEndTimeStampToTag(endTimeStamp:Int, tag: TimeRangeTag)
	{
		tag.endTimeStamp = endTimeStamp;
	}

	static public function createGuid(name:String, timeStamp:Float):String
	{
		var string  = haxe.crypto.Md5.encode(name);
		return  string + Std.int(timeStamp);
	}

	static public function simpleToNumeric(simpleTag:SimpleTag):NumericTag
	{
		return createNewNumericTag(simpleTag.name,simpleTag.timeStamp,Reflect.field(simpleTag, "value"),Reflect.field(simpleTag,"valueType"));
	}

	static public function getSubTagSuggestionsFor(parentTag:SimpleTag, string:String)
	{
		
		var suggestions = new Array<SimpleTag>();

		if(parentTag==null)
		{
			suggestions = suggestions.concat(findTagsWithString(tags,string));
		}
		else
		{
			var ancestors = getAncestors(parentTag);
			ancestors.push(parentTag);


			var filtredTags = findTagsWithName(tags,ancestors[0].name);

			for(i in 1...ancestors.length)
				filtredTags = getSubTagsWithName(filtredTags,ancestors[i].name);

			for(tag in filtredTags)
				suggestions = suggestions.concat(tag.subTags);

			suggestions = findTagsWithString(suggestions,string);
		}
		suggestions = removeNameDuplicates(suggestions);

		if(parentTag != null)
			suggestions = removeTagsWithNames(suggestions, parentTag.subTags);

		return suggestions;
	}

	static function removeTagsWithNames(tags:Array<SimpleTag>, tagsToRemove:Array<SimpleTag>):Array<SimpleTag>
	{
		var result = new Array<SimpleTag>();
		for (tag in tags)
		{
			var found = false;
			for (r in tagsToRemove)
			{
				if(r.name == tag.name)
				{
					found = true;
					break;
				}
			}
			if(!found )
				result.push(tag);

		}
		return result;
	}

	public static function  getSubTagsWithName(parentTags:Array<SimpleTag>, name:String):Array<SimpleTag>
	{
		var filtredTags = new Array<SimpleTag>();
		for (tag in parentTags)
			filtredTags = filtredTags.concat(findTagsWithName(tag.subTags ,name));
		return filtredTags;
	}

	static function toString(tags: Array<SimpleTag>)
	{
		var string = "";
		for(i in 0...tags.length)
			string = string+", "+ tags[i].name;
		trace(string);
	}

	static public function removeNameDuplicates(tags: Array<SimpleTag>)
	{
		var newTags = new Array<SimpleTag>();
		for(tag in tags)
		{
			var found = false;
			for (newTag in newTags)
			{
				if(tag.name == newTag.name)
				{
					found = true;
					break;
				}	
			}
			if(!found)
			{
				newTags.push(tag);
			}
		}
		return newTags;
	}

	static public function findTagsWithString(tags: Array<SimpleTag>, string:String):Array<SimpleTag>
	{
		function tagNameHasString(tag:SimpleTag):Bool
		{
			if(tag.name.toLowerCase().indexOf(string.toLowerCase(),0) != -1)
				return true;
			return false;
		}

		var filtredTags = tags.filter(tagNameHasString);
		return filtredTags;

	}

	static public function findTagsWithName(tags: Array<SimpleTag>, name:String):Array<SimpleTag>
	{
		function haveSameName(tag:SimpleTag):Bool
		{
			if(tag.name == name)
				return true;
			return false;
		}

		var filtredTags = tags.filter(haveSameName);
		return filtredTags;

	}

	static public function getAncestors(lostChild:SimpleTag):Array<SimpleTag>
	{

		var ancestors = new Array<SimpleTag>();
		
		function isParent(parent:SimpleTag):Bool
		{
			var i = parent.subTags.indexOf(lostChild,0);

			if(i==-1)
			{
				for (tag in parent.subTags)
					if(isParent(tag))
					{
						ancestors.push(parent);
						return true;
					}
			}
			else
			{
				ancestors.push(parent);
				return true;
			}

			return false;
		}

		for(tag in tags)
			isParent(tag);

		return ancestors;
	}

	static public function getColor(name:String):Int
	{

		if(!tagsColors.exists(name))
		{	
			trace("new : "+ name);
			return newColor(name);
		}
		else
		{
			trace("get : "+ name);
			return tagsColors.get(name);
		}
		return 0xffffff;
	}

	static function newColor(name:String):Int
	{
		var uniqueTags = getSubTagSuggestionsFor(null,"");
		toString(uniqueTags);
		var color =createColor();//colors[uniqueTags.length];
		tagsColors.set(name, color);
		createColor();
		return color;
	}

	static function createColor():Int
	{
		var tr = 0xff;
		var tg = 0x99;
		var tb = 0x00;
		var alpha = 0.5;
		function make(tint:Int)
		{
			var source = Math.round(Math.random()*0xff);
			return Math.round((tint - source)*alpha + source);
		}

		var red = make(tr);
		var green = make(tg);
		var blue = make(tb);

		var f = Std.parseInt("0x"+StringTools.hex(red,2)+StringTools.hex(green,2)+StringTools.hex(blue,2));
		return f;
		//trace(StringTools.hex(f));
	}

}


typedef SimpleTag = {
	var name : String;
    var timeStamp:Float;
    var subTags: Array <SimpleTag>;
    var guid: String;
    var type: String;
}
typedef NumericTag = {> SimpleTag,
    var value : Float;
    var valueType: String;
}
typedef TimeRangeTag = {> SimpleTag,
    var endTimeStamp:Int;
}

enum TagType {
	Simple;
	Numeric;
	TimeRange;
}


	
